import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AnalysisManagerRoutingModule } from './analysis-manager-routing.module';
import { AnalysisManagerComponent } from './analysis-manager.component';
import { FormsModule } from '@angular/forms';
import { MomentModule } from 'ngx-moment';
import { NgbDatepickerModule, NgbDropdownModule, NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { YopsilonMaskModule } from 'yopsilon-mask';
import { PipesModule } from 'src/app/@core/pipes/pipes.module';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { ComponentsModule } from 'src/app/components/components.module';
import { NgxUpperCaseDirectiveModule } from 'ngx-upper-case-directive';

import { AnalysisDetailsComponent } from './analysis-details/analysis-details.component';
import { MatTooltipModule } from '@angular/material';
import { NgxCurrencyModule } from 'ngx-currency';
import { CoinPairsComponent } from './coin-pairs/coin-pairs.component';
import { ModalCoinPairsComponent } from './coin-pairs/modal-coin-pairs/modal-coin-pairs.component';
import { PlansComponent } from './plans/plans.component';
import { ModalPlansComponent } from './plans/modal-plans/modal-plans.component';
import { ModalGoalsDetailsComponent } from './analysis-details/modal-goals-details/modal-goals-details.component';
import { CuponsComponent } from './cupons/cupons.component';
import { ModalCuponsComponent } from './cupons/modal-cupons/modal-cupons.component';
import { NgxMaskModule } from 'ngx-mask';



@NgModule({
  declarations: [AnalysisManagerComponent, AnalysisDetailsComponent, CoinPairsComponent, ModalCoinPairsComponent, PlansComponent, ModalPlansComponent, ModalGoalsDetailsComponent, CuponsComponent, ModalCuponsComponent],
  imports: [
    CommonModule,
    AnalysisManagerRoutingModule,
    FormsModule,
    MomentModule,
    NgbTooltipModule,
    NgbDropdownModule,
    YopsilonMaskModule,
    PipesModule,
    NgxSkeletonLoaderModule,
    NgbDatepickerModule,
    ComponentsModule,
    MatTooltipModule,
    NgxCurrencyModule,
    NgxUpperCaseDirectiveModule,
    NgxMaskModule.forRoot()
  ]
})
export class AnalysisManagerModule { }
