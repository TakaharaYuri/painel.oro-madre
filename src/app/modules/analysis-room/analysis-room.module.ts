import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AnalysisRoomRoutingModule } from './analysis-room-routing.module';
import { AnalysisRoomComponent } from './analysis-room.component';
import { ComponentsModule } from 'src/app/components/components.module';
import { NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { PipesModule } from 'src/app/@core/pipes/pipes.module';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { FormsModule } from '@angular/forms';
import { CardAnalysisItemComponent } from './card-analysis-item/card-analysis-item.component';
import { AnalysisDetailClientComponent } from './analysis-detail-client/analysis-detail-client.component';
import { AnalysisCommentsComponent } from './analysis-detail-client/analysis-comments/analysis-comments.component';
import { MomentModule } from 'ngx-moment';
import { SubscriptionsComponent } from './subscriptions/subscriptions.component';



@NgModule({
  declarations: [AnalysisRoomComponent, CardAnalysisItemComponent, AnalysisDetailClientComponent, AnalysisCommentsComponent, SubscriptionsComponent],
  imports: [
    CommonModule,
    AnalysisRoomRoutingModule,
    ComponentsModule,
    FormsModule,
    NgbTooltipModule,
    NgxSkeletonLoaderModule,
    PipesModule,
    InfiniteScrollModule,
    MomentModule
  ]
})

export class AnalysisRoomModule { }
