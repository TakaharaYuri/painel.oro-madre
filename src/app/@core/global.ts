import { Injectable, Output } from '@angular/core';
import { Ng2IzitoastService } from 'ng2-izitoast';
import { environment } from 'src/environments/environment';
import { EvoService } from './evo.service';
import { Router } from '@angular/router';
import { catchError, map } from 'rxjs/operators';
import { of } from 'rxjs';
import { HttpEventType, HttpErrorResponse } from '@angular/common/http';

@Injectable()
export class Global {
    public user: any = {};
    public apiURL = environment.apiUrl;
    public loading = false;
    public useToken = true;
    constructor(
        public iziToas: Ng2IzitoastService,
        public service: EvoService,
        public route: Router
    ) {
        this.iziToas.settings({
            backgroundColor: '#c9aa63',
            titleColor: '#FFF',
            messageColor: '#FFF',
            iconColor: '#FFF',
            maxWidth: '300px',
            balloon: true,
            theme: 'dark'
        })
    }

    public isAdmin() {
        if (this.getLoggedUser().type == 'admin') {
            return true
        }
        return false;
    }

    public isManager() {
        if (this.getLoggedUser().type == 'manager') {
            return true
        }
        return false;
    }

    public isClient() {
        if (this.getLoggedUser().type == 'client') {
            return true
        }
        return false;
    }

    public userType() {
        return this.getLoggedUser().type;
    }

    public adminPermission() {
        if (this.getLoggedUser().type != 'admin') {
            this.route.navigate(['/analysis-room'])
        }
    }


    public getNivelBy(nivel) {
        if (this.getLoggedUser().nivel == nivel) {
            return true;
        }
        return false;
    }

    public userId() {
        return this.getLoggedUser().id;
    }

    public paymentStatus() {
        return this.getLoggedUser().plan_situation;
    }

    public getPermissions(filter: any = false) {
        if (filter) {
            const modulos = JSON.parse(localStorage.getItem("permissoes"));
            return modulos.find(item => item.tag === filter);
        }
        return JSON.parse(localStorage.getItem("permissoes"));
    }

    public empresa() {
        let _empresa: any = localStorage.getItem("empresa");
        return (_empresa) ? JSON.parse(_empresa) : null;
    }

    public getLoggedUser() {
        let _user: any = localStorage.getItem("user");
        let _token: any = localStorage.getItem("token");

        if (_user && _user.length > 0) {
            this.user = (_user) ? JSON.parse(_user) : null;
            this.user.token = (_token) ? _token : null;

            return this.user;
        }

        return false;
    }

    public getProfilePhoto() {
        if (this.getLoggedUser().photo) {
            return this.getLoggedUser().photo;
        }
        else {
            return `https://ui-avatars.com/api/?background=c9aa63&name=${this.getLoggedUser().name}&rounded=true&color=fffff`;
        }
    }

    public generateAvatar(name) {
        return `https://ui-avatars.com/api/?background=c9aa63&name=${name}&rounded=true&color=fffff`;
    }

    preLoadImage(entity: any = {}, file) {
        let tmpFile = file.target.files.item(0)
        entity.file = this.uploadImage(tmpFile);

        if (FileReader) {
            var fr = new FileReader();
            fr.onload = () => {
                entity.img = fr.result;
            }

            fr.readAsDataURL(tmpFile);
            console.log('Entity ->', entity);
        }

        else {
            this.iziToas.warning({ title: 'Metodo não compátivel com sua versão' });
        }
    }

    async uploadImage(file: any) {
        const formData = new FormData();
        formData.append('image', file);
        file.inProgress = true;
        this.service.upload(formData).pipe(
            map(event => {
                switch (event.type) {
                    case HttpEventType.UploadProgress:
                        file.progress = Math.round(event.loaded * 100 / event.total);
                        console.log('file', file);
                        break;
                    case HttpEventType.Response:
                        console.log('event', event);
                        return event;
                }
            }),
            catchError((error: HttpErrorResponse) => {
                file.inProgress = false;
                return of(`${file.name} upload failed.`);
            })).subscribe((event: any) => {
                if (typeof (event) === 'object') {
                    //   this.entity.imagem = event.body.fileName;
                    return event.body;
                }
            });
    }

    async loadImage(image, entity: any) {
        let retultData: any = {};

        let tmpFile = image.target.files.item(0)

        if (FileReader) {
            var fr = new FileReader();
            fr.onload = () => {
                retultData.tmp = fr.result;
            }

            fr.readAsDataURL(tmpFile);
            return entity = await this.uploadImage(tmpFile);
        }

        else {
            this.iziToas.warning({ title: 'Metodo não compátivel com sua versão' });
        }

        return retultData;
    }

    public logout() {
        this.route.navigate(['/login']);
        localStorage.clear();
        location.reload();

    }

    public isValidUrl(url) {
        var pattern = new RegExp('^(https?:\\/\\/)?' + // protocol
            '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
            '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
            '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
            '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
            '(\\#[-a-z\\d_]*)?$', 'i'); // fragment locator
        return !!pattern.test(url);
    }

    public cpfValidate(cpf) {
        cpf = cpf.replace(/\D/g, '');
        if (cpf.toString().length != 11 || /^(\d)\1{10}$/.test(cpf)) return false;
        var result = true;
        [9, 10].forEach(function (j) {
            var soma = 0, r;
            cpf.split(/(?=)/).splice(0, j).forEach(function (e, i) {
                soma += parseInt(e) * ((j + 2) - (i + 1));
            });
            r = soma % 11;
            r = (r < 2) ? 0 : 11 - r;
            if (r != cpf.substring(j, j + 1)) result = false;
        });
        return result;
    }

    public serialize = function (obj) {
        var str = [];
        for (var p in obj)
            if (obj.hasOwnProperty(p)) {
                str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
            }
        return str.join("&");
    }

}
