import { Component, OnInit, ElementRef, OnChanges, SimpleChanges } from '@angular/core';
import { ROUTES } from '../sidebar/sidebar.component';
import { Location, LocationStrategy, PathLocationStrategy } from '@angular/common';
import { Router } from '@angular/router';
import { Global } from 'src/app/@core/global';
import { EvoService } from 'src/app/@core/evo.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
  providers: [EvoService]
})
export class NavbarComponent implements OnInit {
  public focus;
  public listTitles: any[];
  public location: Location;
  public user;
  public breadcrumbs;

  constructor(
    location: Location,
    private element: ElementRef,
    private router: Router,
    public global: Global,
    public service: EvoService
  ) {
    this.location = location;
    // this.breadcrumbs = this.location.prepareExternalUrl(this.location.path()).split('/');
    this.router.events.subscribe(event => {
      this.breadcrumbs = this.location.prepareExternalUrl(this.location.path()).split('/');
    })
  }

  ngOnInit() {
    this.listTitles = ROUTES.filter(listTitle => listTitle);
    this.user = this.global.getLoggedUser();
  }

  getTitle() {
    // var titlee:any = this.location.prepareExternalUrl(this.location.path());
    // if (titlee.charAt(0) === '#') {
    //   titlee = titlee.slice(1);
    // }

    // for (var item = 0; item < this.listTitles.length; item++) {
    //   console.log('[getTitle] ->', this.listTitles[item].path);
    //   if (this.listTitles[item].path === titlee) {
    //     return this.listTitles[item].title;
    //   }
    // }
    var arrayUrls = this.location.prepareExternalUrl(this.location.path()).split('/');
    return arrayUrls[1];
  }
  

}
