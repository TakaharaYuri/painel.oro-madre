import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdministradorRoutingModule } from './administrador-routing.module';
import { AdministradorComponent } from './administrador.component';
import { ComponentsModule } from 'src/app/components/components.module';
import { PipesModule } from 'src/app/@core/pipes/pipes.module';
import { FormsModule } from '@angular/forms';
import { NgbTooltipModule, NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { YopsilonMaskModule } from 'yopsilon-mask';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { ModulosComponent } from './modulos/modulos.component';
import { ModalModulosComponent } from './modulos/modal-modulos/modal-modulos.component';
import { MomentModule } from 'ngx-moment';


@NgModule({
  imports: [
    CommonModule,
    AdministradorRoutingModule,
    ComponentsModule,
    PipesModule,
    FormsModule,
    NgbTooltipModule,
    NgbDropdownModule,
    YopsilonMaskModule,
    NgxSkeletonLoaderModule,
    MomentModule
  ],
  declarations: [
    AdministradorComponent,
    ModulosComponent,
    ModalModulosComponent
  ],
})
export class AdministradorModule { }
