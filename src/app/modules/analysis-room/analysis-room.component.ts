import { Component, OnInit } from '@angular/core';
import { EvoService } from 'src/app/@core/evo.service';
import { Global } from 'src/app/@core/global';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-analysis-room',
  templateUrl: './analysis-room.component.html',
  styleUrls: ['./analysis-room.component.css']
})
export class AnalysisRoomComponent implements OnInit {

  public paginate = {
    page: 1,
    perPage: 0,
    total: 0,
    lastPage: 0
  };
  public data;
  public filter: any = {};
  public loading = false;
  public isFilter = false;;
  public users;
  public coinPairs;
  constructor(
    public global: Global,
    public service: EvoService
  ) { }

  ngOnInit(): void {
    this.get();
    this.getUsers();
    this.getCoinPairs();
  }


  get(search: any = {}) {
    this.isFilter = (Object.keys(search).length > 0) ? true : false;
    console.log('isFilter', search);
    this.service.entityName = `analysis-room/analysis?${this.global.serialize(search)}`;
    this.service.getResources().subscribe((response: any) => {
      this.data = response.data;
      this.setPaginate(response);
    })
  }
  onScroll() {
    if (this.global.paymentStatus().includes('paid','trialing')) {
      this.service.entityName = `analysis-room/analysis?page=${this.paginate.page + 1}&${this.global.serialize(this.filter)}`;
      this.service.getResources().subscribe((response: any) => {
        if (response && response.data) {
          response.data.map(item => {
            this.data.push(item);
          })
        }

        this.setPaginate(response);

      })
    }
    else {
      Swal.fire({
        title: 'Olá Trader',
        text: 'Percebi que você ainda não é um assinante da nossa plataforma, por este motivo você só consegue ter acesso a uma análise. Torne-se um assinante agora mesmo e tenha acesso as melhores análises 24hrs por dia.',
        icon: 'info',
        confirmButtonText: "Assinar agora mesmo!"
      });
    }
  }

  setPaginate(item) {
    this.paginate.perPage = item.perPage;
    this.paginate.page = item.page;
    this.paginate.total = item.total;
    this.paginate.lastPage = item.lastPage;
  }

  getUsers() {
    this.service.entityName = 'users';
    this.service.getResources().subscribe(response => {
      this.users = response;
    })
  }

  getCoinPairs() {
    this.service.entityName = 'coin-pairs';
    this.service.getResources().subscribe((response: any) => {
      this.coinPairs = response;
    })
  }

  clear() {
    this.get();
    this.filter = {};
    this.isFilter = false;
  }

}
