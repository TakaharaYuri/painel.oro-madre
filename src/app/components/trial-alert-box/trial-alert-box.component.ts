import { Component, OnInit } from '@angular/core';
import { EvoService } from 'src/app/@core/evo.service';
import { Global } from 'src/app/@core/global';

@Component({
  selector: 'app-trial-alert-box',
  templateUrl: './trial-alert-box.component.html',
  styleUrls: ['./trial-alert-box.component.css']
})
export class TrialAlertBoxComponent implements OnInit {

  public user;
  constructor(
    public global: Global,
    public service: EvoService 
  ) {
    if (this.global.getLoggedUser().trial_enabled === 1) {
      this.getTrialTime();
    }
  }

  ngOnInit(): void {
    
  }

  getTrialTime() {
    this.service.entityName = 'logged-user';
    this.service.getResources().subscribe(response => {
      this.user = response;
    })
  }

}
