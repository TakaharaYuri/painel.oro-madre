import { Component, OnInit } from '@angular/core';
import { Global } from 'src/app/@core/global';
import { EvoService } from 'src/app/@core/evo.service';
import { HttpEventType, HttpErrorResponse } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';
import { of } from 'rxjs';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css'],
  providers: [EvoService]
})
export class PerfilComponent implements OnInit {

  public data;
  public entity: any = {};
  public img;

  constructor(
    public service: EvoService,
    public global: Global
  ) { }

  ngOnInit(): void {
    this.getProfile();
  }

  getProfile() {
    this.service.entityName = 'perfil';
    this.service.getResources().subscribe(response => {
      console.log('Response ->', response);
      this.data = response;
      this.entity = this.data;
    });
  }

  public save() {
    this.service.save(this.entity).subscribe( response => {
      this.global.iziToas.success({title:'Perfil atualizado com sucesso'});
    })
  }

  loadImage(image) {
    let tmpFile = image.target.files.item(0)
    this.uploadImage(tmpFile);

    if (FileReader) {
      var fr = new FileReader();
      fr.onload = () => {
        this.img = fr.result;
      }

      fr.readAsDataURL(tmpFile);
    }

    else {
      this.global.iziToas.warning({ title: 'Metodo não compátivel com sua versão' });
    }
  }

  uploadImage(file) {
    const formData = new FormData();
    formData.append('image', file);
    file.inProgress = true;
    this.service.upload(formData).pipe(
      map(event => {
        switch (event.type) {
          case HttpEventType.UploadProgress:
            file.progress = Math.round(event.loaded * 100 / event.total);
            console.log('file', file);
            break;
          case HttpEventType.Response:
            console.log('event', event);
            return event;
        }
      }),
      catchError((error: HttpErrorResponse) => {
        file.inProgress = false;
        return of(`${file.name} upload failed.`);
      })).subscribe((event: any) => {
        if (typeof (event) === 'object') {
          this.entity.imagem = event.body.fileName;
          this.service.save(this.entity).subscribe(
            response => {
              this.global.iziToas.success({ title: 'Imagem de perfil atualizada' });
            }
          );
        }
      });
  }

}
