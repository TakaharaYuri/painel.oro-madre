import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { EvoService } from 'src/app/@core/evo.service';
import { Global } from 'src/app/@core/global';

@Component({
  selector: 'app-analysis-comments',
  templateUrl: './analysis-comments.component.html',
  styleUrls: ['./analysis-comments.component.css']
})
export class AnalysisCommentsComponent implements OnInit {

  public comments;
  public loading = false;
  public comment;
  @Input() analysis_id;
  @Output() commentsCount =  new EventEmitter();
  constructor(
    public global: Global,
    public service: EvoService
  ) {
    this.service.entityName = `analysis-room/comments`;
  }

  ngOnInit(): void {
    if (this.analysis_id) {
      this.get(this.analysis_id);
    }
  }

  get(analysis_id) {
    this.service.getResource(analysis_id).subscribe(response => {
      this.comments = response;
      this.commentsCount.emit(this.comments.length);
    });
  }

  save(comment) {
    if (comment.length > 10) {

      const normalize = {
        comment: comment,
        analysis_id: this.analysis_id
      }

      this.service.createResource(normalize).subscribe((response: any) => {
        this.loading = true;
        if (response.result == true) {
          this.comment = null;
          this.comments.unshift(response.data);
          this.commentsCount.emit(this.comments.length);
        }
      })
    }
    else {
      this.global.iziToas.show({
        title: 'Ops',
        message: 'Seu comentário precisa ter no mínimo 10 caracteres...'
      })
    }
  }

  delete(item) {
    this.service.onDeleteConfirm(item.id).then(confirm => {
      if (confirm) {
        confirm.subscribe((response: any) => {
          if (response.result == true) {
            const index = this.comments.indexOf(item);
            console.log('index', index);
            this.comments.splice(index, 1);
            this.commentsCount.emit(this.comments.length);
            this.global.iziToas.success({ title: 'Registro removido com sucesso' });
          }
          else {
            this.global.iziToas.success({ title: response.message });
          }
        })
      }
    })
  }

  onEdit(item) {
    item.edit = (item.edit === true) ? false : true;
  }

  update(item) {
    item.edited = true;
    this.service.updateResource(item).subscribe((response: any) => {
      console.log(response);
      item.edit = false;
    })
  }

}
