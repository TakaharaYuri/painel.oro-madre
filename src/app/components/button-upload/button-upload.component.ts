import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { catchError, map } from 'rxjs/operators';
import { of } from 'rxjs';
import { HttpEventType, HttpErrorResponse } from '@angular/common/http';
import { EvoService } from 'src/app/@core/evo.service';
import { Global } from 'src/app/@core/global';

@Component({
  selector: 'button-upload',
  templateUrl: './button-upload.component.html',
  styleUrls: ['./button-upload.component.css']
})
export class ButtonUploadComponent implements OnInit {

  @Output() event: EventEmitter<any> = new EventEmitter<any>();;
  @Output() tmpFile;
  @Output() progress;
  @Output() loading = false;
  @Input() accept = 'image/*'
  @Input() typeUpload = 'image';
  @Input() text = "Selecionar";
  @Input() icon = 'ni ni-image'
  @Input() checked = false;
  @Input() customClass;
  @Input() disabled = false;
  constructor(
    private service: EvoService,
    private global: Global
  ) { }

  ngOnInit(): void {
  }


  public loadFile(file) {
    if (this.typeUpload == 'image') {
      this.loading = true;
      let tmpFile = file.target.files.item(0)
      this.upload(tmpFile);
      if (FileReader) {
        var fr = new FileReader();
        fr.onload = () => {
          this.tmpFile = fr.result;
        }

        fr.readAsDataURL(tmpFile);

      }

      else {
        this.global.iziToas.warning({ title: 'Metodo não compátivel com sua versão' });
      }
    }
    else {
      this.loading = true;
      let tmpFile = file.target.files.item(0);
      console.log(tmpFile);
      if (tmpFile.type === this.accept) {
        this.upload(tmpFile);
      }
      else {
        this.loading = false;
        this.global.iziToas.error({
          title:"Ops",
          message:'O tipo de arquivo que você está tentando enviar não é válido, verifique e tente novamente.'
        })
      }
    }
  }

  upload(file) {
    const formData = new FormData();
    formData.append('image', file);
    this.service.upload(formData).pipe(
      map(event => {
        switch (event.type) {
          case HttpEventType.UploadProgress:
            file.progress = Math.round(event.loaded * 100 / event.total);
            this.progress = file.progress;
            break;
          case HttpEventType.Response:
            return event;
        }
      }),
      catchError((error: HttpErrorResponse) => {
        file.inProgress = false;
        return of(`${file.name} upload failed.`);
      })).subscribe((event: any) => {
        if (typeof (event) === 'object') {
          this.event.emit(event.body.fileName);
          this.loading = false;
        }
      });
  }


  clear() {
    this.event.emit(null);
  }

}
