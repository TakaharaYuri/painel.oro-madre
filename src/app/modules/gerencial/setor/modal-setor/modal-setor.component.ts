import { Component, OnInit, Input } from '@angular/core';
import { EvoService } from 'src/app/@core/evo.service';
import { Global } from 'src/app/@core/global';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-modal-setor',
  templateUrl: './modal-setor.component.html',
  styleUrls: ['./modal-setor.component.css']
})
export class ModalSetorComponent implements OnInit {

  @Input() entity: any = {

  };
  public modules;
  public selecteModules: any = [];
  public permissions: any = [];


  constructor(
    public global: Global,
    public service: EvoService,
    private activeModal: NgbActiveModal,
  ) { }

  ngOnInit(): void {
    this.getModules();
    if (!this.entity.permissions) {
      this.entity.permissions = [];
    }
    else {
      this.selecteModules = this.entity.permissions;
    }

  }

  save() {
    this.service.entityName = 'setor';
    this.entity.permissions = this.selecteModules;
    this.service.save(this.entity).subscribe(response => {
      this.global.iziToas.success({ title: 'Setor cadastrado com Sucesso' });
      this.activeModal.close(true);
    },
      error => {
        this.global.iziToas.error({ title: 'Ocorreu um problema ao processar sua requisição' });
      })
  }

  close() {
    this.activeModal.close(false);
  }

  getModules() {
    this.service.entityName = 'modulos-arvore';
    this.service.getResources().subscribe(response => {
      this.modules = response;
      this.checkModuleIsSelected();
    })
  }

  setSelectedModule(checkbox, item) {
    const normalize = {
      id: item.id,
      nome: item.nome
    }

    const _removedDuplicateModules = {};
    this.selecteModules = this.selecteModules.filter(module => !_removedDuplicateModules[module.id] && (_removedDuplicateModules[module.id] = true));
    const moduleIndex = this.selecteModules.findIndex(module => module.id == normalize.id);

    if (checkbox == true && !moduleIndex || moduleIndex == -1) {
      this.selecteModules.push(normalize);
    }
    else {
      this.selecteModules.splice(moduleIndex, 1);
    }

    // console.log(this.selecteModules);
  }


  checkModuleIsSelected() {
    if (this.selecteModules) {
      this.selecteModules.map(item => {
        console.log(item);
        this.permissions[item.id] = true;
      });
    }
  }

}
