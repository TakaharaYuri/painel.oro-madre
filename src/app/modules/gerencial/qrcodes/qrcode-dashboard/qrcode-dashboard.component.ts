import { Component, OnInit } from '@angular/core';
import { EvoService } from 'src/app/@core/evo.service';
import { Global } from 'src/app/@core/global';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-qrcode-dashboard',
  templateUrl: './qrcode-dashboard.component.html',
  styleUrls: ['./qrcode-dashboard.component.css']
})
export class QrcodeDashboardComponent implements OnInit {

  public id;
  public data;

  constructor(
    public global: Global,
    public service: EvoService,
    public route: ActivatedRoute
  ) {
    this.route.params.subscribe(param => {
      if (param.id) {
        this.id = param.id;
        this.get();
      }
    })
  }

  ngOnInit(): void {
  }
  get() {
    this.service.entityName = `relatorios/qrcode`;
    this.service.getResource(this.id).subscribe(response => {
      this.data = response;
      console.log(response);
    })
  }

}
