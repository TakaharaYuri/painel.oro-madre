import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { EvoService } from 'src/app/@core/evo.service';
import { Global } from 'src/app/@core/global';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-registro-cadastro',
  templateUrl: './registro-cadastro.component.html',
  styleUrls: ['./registro-cadastro.component.css']
})
export class RegistroCadastroComponent implements OnInit {

  public entity: any = {
    address: {}
  };
  public onError = {
    cpf: false
  }
  public loading = false;
  public passwordConfirmation;
  @BlockUI() blockUI: NgBlockUI;

  // public session;

  constructor(
    public global: Global,
    public service: EvoService,
    public route: ActivatedRoute,
    public router: Router
  ) { 
    if (sessionStorage.getItem('customer')) {
      this.entity = JSON.parse(sessionStorage.getItem('customer'));
    }
  }

  ngOnInit(): void {
  }

  save() {
    sessionStorage.setItem('customer', JSON.stringify(this.entity));
    this.router.navigate(['/plan-select']);
  }


  onChangeCEP(cep) {
    this.blockUI.start('Buscando informações do endereço')
    this.service.getCep(cep).subscribe(
      (response:any) => {
      if (response) {
        this.entity.address.street = response.logradouro;
        this.entity.address.neighborhood = response.bairro;
      }
      this.blockUI.stop();
    },
    (error) => {
      this.blockUI.stop();
      this.global.iziToas.error({
        title:'Ops',
        message:'Ocorreu um problema ao buscar o cep informado.'
      })
    })
  }


  onChangeEmail(email) {
    this.blockUI.start('Consultando informações...');
    this.onError.cpf = false;
    this.service.entityName = `validate-informations?field=email&value=${email}`;
    this.service.getResources().subscribe((response: any) => {
      this.blockUI.stop();
      if (response && response.result == true) {
        Swal.fire({
          title: 'Ops!',
          icon: 'warning',
          text: 'Já existe um usuário cadastrado com o E-mail informado, tente recuperar sua senha ou entre em contato com o suporte da Plataforma',
          confirmButtonText: 'Recuperar Senha',
        }).then(result => {
          if (!result.value) {
            this.router.navigate(['/login'])
          }
        })
      }
    })
  }

  onChangeWhatsapp(whatsapp) {
    if (whatsapp.length >= 11) {
      this.blockUI.start('Consultando informações...');
      this.onError.cpf = false;
      this.service.entityName = `validate-informations?field=phone&value=${whatsapp}`;
      this.service.getResources().subscribe((response: any) => {
        this.blockUI.stop();
        if (response && response.result == true) {
          Swal.fire({
            title: 'Ops!',
            icon: 'warning',
            text: 'Já existe um usuário cadastrado com o Telefone informado, tente recuperar sua senha ou entre em contato com o suporte da Plataforma',
            confirmButtonText: 'Recuperar Senha',
          }).then(result => {
            if (!result.value) {
              this.router.navigate(['/login'])
            }
          })
        }
      })
    }
  }

  onChangeCPF(cpf) {
    if (cpf.length >= 11) {
      this.blockUI.start('Consultando informações...');
      if (this.global.cpfValidate(cpf)) {
        this.onError.cpf = false;
        this.service.entityName = `validate-informations?field=cpf&value=${cpf}`;
        this.service.getResources().subscribe((response: any) => {
          this.blockUI.stop();
          if (response && response.result == true) {
            Swal.fire({
              title: 'Ops!',
              icon: 'warning',
              text: 'Já existe um usuário cadastrado com o CPF informado, tente recuperar sua senha ou entre em contato com o suporte da Plataforma',
              confirmButtonText: 'Recuperar Senha',
            }).then(result => {
              if (!result.value) {
                this.router.navigate(['/login'])
              }
            })
          }
        })
      }
      else {
        this.global.iziToas.error({
          title: 'O CPF informado é inválido'
        });
        this.onError.cpf = true;
        this.blockUI.stop();
      }
    }
  }

}
