import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SidebarComponent } from './sidebar/sidebar.component';
import { NavbarComponent } from './navbar/navbar.component';
import { FooterComponent } from './footer/footer.component';
import { RouterModule } from '@angular/router';
import { NgbModule, NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { PipesModule } from '../@core/pipes/pipes.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule } from '@angular/forms';
import { CardModuleComponent } from './button-upload/card-module/card-module.component';
import { PageHeaderComponent } from './page-header/page-header.component';
import { FabButtonComponent } from './fab-button/fab-button.component';
import { EmptyDataComponent } from './empty-data/empty-data.component';
import { CardDashboardComponent } from './card-dashboard/card-dashboard.component';
import { ButtonUploadComponent } from './button-upload/button-upload.component';
import { FormFieldRowComponent } from './form-field-row/form-field-row.component';
import { PageCardComponent } from './page-card/page-card.component';
import { CardUserProfileComponent } from './card-user-profile/card-user-profile.component';
import { MomentModule } from 'ngx-moment';
import { PaginatorComponent } from './paginator/paginator.component';
import { TrialAlertBoxComponent } from './trial-alert-box/trial-alert-box.component';
import { MatTooltipModule } from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    NgbModule,
    PipesModule,
    NgSelectModule,
    NgbTooltipModule,
    MomentModule,
    MatTooltipModule
  ],
  declarations: [
    FooterComponent,
    NavbarComponent,
    SidebarComponent,
    CardModuleComponent,
    PageHeaderComponent,
    FabButtonComponent,
    EmptyDataComponent,
    CardDashboardComponent,
    ButtonUploadComponent,
    FormFieldRowComponent,
    PageCardComponent,
    CardUserProfileComponent,
    PaginatorComponent,
    TrialAlertBoxComponent
  ],
  exports: [
    FooterComponent,
    NavbarComponent,
    SidebarComponent,
    CardModuleComponent,
    PageHeaderComponent,
    FabButtonComponent,
    EmptyDataComponent,
    CardDashboardComponent,
    ButtonUploadComponent,
    FormFieldRowComponent,
    PageCardComponent,
    CardUserProfileComponent,
    PaginatorComponent,
    TrialAlertBoxComponent
  ]
})
export class ComponentsModule { }
