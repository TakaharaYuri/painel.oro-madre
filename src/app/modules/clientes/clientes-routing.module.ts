import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClientesComponent } from './clientes.component';
import { ClientsDetailsComponent } from './clients-details/clients-details.component';


const routes: Routes = [
  { path: '', component: ClientesComponent },
  { path: 'details/:id', component: ClientsDetailsComponent },
  { path: 'new', component: ClientsDetailsComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClientesRoutingModule { }
