import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AuthLayoutRoutes } from './auth-layout.routing';

import { LoginComponent } from '../../pages/login/login.component';
import { AutenticacaoModule } from 'src/app/modules/autenticacao/autenticacao.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgxMaskModule } from 'ngx-mask';
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AuthLayoutRoutes),
    FormsModule,
    NgxMaskModule.forRoot(),
    AutenticacaoModule
    // NgbModule
  ],
  declarations: [
    LoginComponent,
    // RegisterComponent
  ]
})
export class AuthLayoutModule { }
