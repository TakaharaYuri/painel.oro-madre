import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ClientesRoutingModule } from './clientes-routing.module';
import { ClientesComponent } from './clientes.component';
import { FormsModule } from '@angular/forms';
import { NgbTooltipModule, NgbDropdownModule, NgbDatepickerModule, NgbNavModule, NgbAccordionModule } from '@ng-bootstrap/ng-bootstrap';
import { MomentModule } from 'ngx-moment';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { PipesModule } from 'src/app/@core/pipes/pipes.module';
import { ComponentsModule } from 'src/app/components/components.module';
import { YopsilonMaskModule } from 'yopsilon-mask';
import { NgxCurrencyModule } from 'ngx-currency';
import { ClientsDetailsComponent } from './clients-details/clients-details.component';
import { NgxMaskModule } from 'ngx-mask';


@NgModule({
  declarations: [ClientesComponent, ClientsDetailsComponent],
  imports: [
    CommonModule,
    ClientesRoutingModule,
    FormsModule,
    MomentModule,
    NgbTooltipModule,
    NgbDropdownModule,
    YopsilonMaskModule,
    PipesModule,
    NgxSkeletonLoaderModule,
    NgbDatepickerModule,
    ComponentsModule,
    NgbNavModule,
    NgxCurrencyModule,
    NgbAccordionModule,
    NgxMaskModule.forRoot()
  ]
})
export class ClientesModule { }
