import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import Chart from 'chart.js';
import { EvoService } from 'src/app/@core/evo.service';
import { Global } from 'src/app/@core/global';

// core components
import {
  chartOptions,
  parseOptions,
  chartExample1,
  chartExample2
} from "../../variables/charts";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  public datasets: any;
  public data: any;
  public salesChart;
  public clicked: boolean = true;
  public clicked1: boolean = false;
  public dataGraph;

  constructor(
    public global: Global,
    public service: EvoService,
    public router: Router) {
    this.global.adminPermission();
  }

  ngOnInit() {
    this.get();
    // this.datasets = [
    //   [0, 20, 10, 30, 15, 40, 20, 60, 60],
    //   [0, 20, 5, 25, 10, 30, 15, 40, 40]
    // ];

    // this.dataGraph = this.datasets[0];


    // var chartOrders = document.getElementById('chart-orders');

    // parseOptions(Chart, chartOptions());


    // var ordersChart = new Chart(chartOrders, {
    //   type: 'bar',
    //   options: chartExample2.options,
    //   data: chartExample2.data
    // });

    // var chartSales = document.getElementById('chart-sales');

    // this.salesChart = new Chart(chartSales, {
    //   type: 'line',
    //   options: chartExample1.options,
    //   data: chartExample1.data
    // });

    // data: {
    //   labels: ['Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
    //   datasets: [{
    //     label: 'Performance',
    //     data: [0, 20, 10, 30, 15, 40, 20, 60, 0]
    //   }]
    // }
  }

  get() {
    this.service.entityName = 'dashboard';
    this.service.getResources().subscribe(response => {
      this.data = response;
    });
  }







  public updateOptions() {
    this.salesChart.data.datasets[0].data = this.dataGraph;
    this.salesChart.update();
  }

}
