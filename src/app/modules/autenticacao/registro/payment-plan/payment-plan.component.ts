import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { EvoService } from 'src/app/@core/evo.service';
import { Global } from 'src/app/@core/global';
import Swal from 'sweetalert2';



@Component({
  selector: 'app-payment-plan',
  templateUrl: './payment-plan.component.html',
  styleUrls: ['./payment-plan.component.css']
})
export class PaymentPlanComponent implements OnInit {

  public entity: any = {};
  public paymentMethod;
  public plan;
  @BlockUI() blockUI: NgBlockUI;

  constructor(
    public global: Global,
    public service: EvoService,
    public route: ActivatedRoute,
    public router: Router
  ) {
    if (sessionStorage.getItem('customer')) {
      this.entity = JSON.parse(sessionStorage.getItem('customer'));
      this.plan = JSON.parse(sessionStorage.getItem('plan'));
    }
    this.route.params.subscribe(params => {
      if (params.payment_method) {
        this.paymentMethod = params.payment_method;
      }
      else {
        Swal.fire({
          title: 'Ops!',
          icon: 'warning',
          text: 'Não foi possível processar sua solicitação, verifique a URL e tente novamente..',
          confirmButtonText: 'Continuar',
        }).then(result => {
          if (!result.value) {
            this.router.navigate(['/login'])
          }
        })
      }
    })

    if (sessionStorage.getItem('customer')) {

    }
    else {
      Swal.fire({
        title: 'Ops!',
        icon: 'warning',
        text: 'Você ainda não completou os seus dados cadstrais, volte e tente novamente.',
        confirmButtonText: 'Voltar',
      }).then(result => {
        if (!result.value) {
          this.router.navigate(['/register'])
        }
      })
    }
  }

  ngOnInit(): void { }

  public processTransation() {
    this.blockUI.start('Estamos processando seu pagamento....')
    this.service.entityName = 'payment-gateway/credit-card/process';
    this.entity.plan_id = this.plan.id;
    this.service.createResource(this.entity).subscribe(
      (response: any) => {
        if (response && response.result == true) {
          Swal.fire({
            title: 'Pronto!',
            icon: 'success',
            text: response.message,
            confirmButtonText: 'Continuar'
          }).then((result) => {
            if (result.value) {
              this.router.navigate(['/login'])
            }
          })
        }
        else {
          Swal.fire({
            title: 'Ops!',
            icon: 'error',
            text: response.message,
            confirmButtonText: 'Continuar'
          });
        }

        this.blockUI.stop();
      },
      error => {
        Swal.fire({
          title: 'Ops!',
          icon: 'error',
          text: 'Ocorreu um erro ao processa sua solicitação, entre em contato com o suporte para mais informações.',
          confirmButtonText: 'Continuar'
        });
        this.blockUI.stop();
      });
  }

  getPlan() {
    this.service.entityName = 'active_plan';
    this.service.getResources().subscribe(response => {
      this.plan = response;
    })
  }

}
