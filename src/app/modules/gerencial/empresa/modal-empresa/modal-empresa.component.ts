import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Global } from 'src/app/@core/global';
import { EvoService } from 'src/app/@core/evo.service';
import { HttpEventType, HttpErrorResponse } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';
import { of } from 'rxjs';

@Component({
  selector: 'app-modal-empresa',
  templateUrl: './modal-empresa.component.html',
  styleUrls: ['./modal-empresa.component.css']
})
export class ModalEmpresaComponent implements OnInit {
  constructor(
    private activeModal: NgbActiveModal,
    private global: Global,
    private service: EvoService
  ) {
    this.service.entityName = 'empresa';
  }

  @Input() entity: any = {};
  public img;

  ngOnInit(): void {
  }

  close() {
    this.activeModal.close();
  }

  save() {
    // console.log(this.entity);
    this.service.save(this.entity).subscribe(
      response => {
        this.global.iziToas.success({title:'Dados salvos com sucesso'});
        this.close();
      },
      error => {
        this.global.iziToas.error({ title: 'Ocorreu um problema ao processar sua solicitação' });
      })
  }

  loadImage(image) {
    let tmpFile = image.target.files.item(0)
    this.uploadImage(tmpFile);

    if (FileReader) {
      var fr = new FileReader();
      fr.onload = () => {
        this.img = fr.result;
      }

      fr.readAsDataURL(tmpFile);
    }

    else {
      this.global.iziToas.warning({ title: 'Metodo não compátivel com sua versão' });
    }
  }

  uploadImage(file) {
    const formData = new FormData();
    formData.append('image', file);
    file.inProgress = true;
    this.service.upload(formData).pipe(
      map(event => {
        switch (event.type) {
          case HttpEventType.UploadProgress:
            file.progress = Math.round(event.loaded * 100 / event.total);
            console.log('file', file);
            break;
          case HttpEventType.Response:
            console.log('event', event);
            return event;
        }
      }),
      catchError((error: HttpErrorResponse) => {
        file.inProgress = false;
        return of(`${file.name} upload failed.`);
      })).subscribe((event: any) => {
        if (typeof (event) === 'object') {
          this.entity.logo = event.body.fileName;
        }
      });
  }

  generateIdentifier(text) {
    text = text.replace(/[^\w\s]/gi, '');
    text = text.split('  ').join(' ');
    text = text.split('-').join('');
    text = text.split(' ').join('-');
    text = text.split('--').join('-');
    text = text.toLowerCase();
    console.log(text);
    this.entity.identificador = text.split(' ').join('-');
  }

}
