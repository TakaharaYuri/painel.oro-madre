import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-empty-data',
  templateUrl: './empty-data.component.html',
  styleUrls: ['./empty-data.component.css']
})
export class EmptyDataComponent implements OnInit {
  @Input() text = 'Nenhum dado cadastrado';
  @Input() description = '';
  @Input() image = 'assets/img/icons/empty.svg';
  @Input() ctaRoute;
  @Input() ctaText = 'Cadastrar-se';
  @Input() ctaIcon = 'fa fa-check'
  constructor() { }

  ngOnInit(): void {
  }

}
