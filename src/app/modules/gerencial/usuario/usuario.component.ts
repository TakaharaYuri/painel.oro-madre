import { Component, OnInit } from '@angular/core';
import { Global } from 'src/app/@core/global';
import { EvoService } from 'src/app/@core/evo.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalUsuarioComponent } from './modal-usuario/modal-usuario.component';
import { ModalConviteComponent } from './modal-convite/modal-convite.component';

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.css']
})
export class UsuarioComponent implements OnInit {

  public data;

  constructor(
    public global: Global,
    public service: EvoService,
    private modalService: NgbModal
  ) {

  }

  ngOnInit(): void {
    this.get();
  }

  get() {
    this.service.entityName = 'users';
    this.service.getResources().subscribe(response => {
      this.data = response;
      console.log('Response ->', response);
    })
  }

  open(data = null) {
    const modal = this.modalService.open(ModalUsuarioComponent, {
      size: 'lg',
      keyboard: false,
      backdrop: 'static'

    });
    modal.componentInstance.entity = data;
    modal.result.then((result) => {
      this.get();
    });
  }

  openModalConvite() {
    const modal = this.modalService.open(ModalConviteComponent, {
      size: 'md',
      keyboard: false,
      backdrop: 'static'
    });
    // modal.componentInstance.entity = data;
    modal.result.then((result) => {
      this.get();
    });
  }

  updateFila(item) {
    if (item.ativo_fila == 1) item.ativo_fila = 0;
    else if (item.ativo_fila == 0) item.ativo_fila = 1;

    this.service.entityName = 'usuario';
    this.service.save(item).subscribe(response => {
      this.global.iziToas.success({ title: 'Registro atualizado com sucesso' });
      this.get();
    })
  }

  updateStatus(item) {
    if (item.status == 1) item.status = 0;
    else if (item.status == 0) item.status = 1;

    this.service.entityName = 'usuario';
    this.service.save(item).subscribe(response => {
      this.global.iziToas.success({ title: 'Registro atualizado com sucesso' });
      this.get();
    })
  }

  delete(id) {
    this.service.onDeleteConfirm(id, 'Todas as informações ligadas a ele também serão removidas.').then(confirm => {
      if (confirm) {
        confirm.subscribe(response => {
          console.log('DELETE', response);
          this.global.iziToas.success({ title: 'Registro removido com sucesso' });
          this.get();
        })
      }
    })
  }

}
