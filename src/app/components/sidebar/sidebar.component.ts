import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Global } from 'src/app/@core/global';
import { EvoService } from 'src/app/@core/evo.service';
import { toInteger } from '@ng-bootstrap/ng-bootstrap/util/util';

declare interface RouteInfo {
  path: string;
  title: string;
  icon: string;
  class: string;
  nivel: string;
}
export const ROUTES: RouteInfo[] = [
  // { path: '/dashboard', title: 'Dashboard',  icon: 'ni-chart-bar-32 text-blue', class: '' },
  { path: '/dashboard', title: 'Dashboard', icon: 'fa fa-tachometer-alt text-primary', class: '', nivel: 'admin' },
  // { path: '/gerencial/usuarios', title: 'Meu Perfil', icon: 'ni ni-single-02 text-primary', class: '', nivel: 'admin' },
  // { path: '/gerencial/usuarios', title: 'Usuários', icon: 'ni ni-single-02 text-primary', class: '', nivel: 'admin' },
  { path: '/analysis-manager/plans', title: 'Planos', icon: 'fa fa-money-check-alt text-primary', class: '', nivel: 'admin' },
  // { path: '/analysis-manager/cupons', title: 'Cupons de Desconto', icon: 'ni ni-bag-17 text-primary', class: '', nivel: 'admin' },
  { path: '/clients', title: 'Clientes', icon: 'fa fa-users text-primary', class: '', nivel: 'admin' },
  { path: '/analysis-manager/coin-pairs', title: 'Pares de Moedas', icon: 'ni ni-money-coins text-primary', class: '', nivel: 'admin' },
  { path: '/analysis-manager', title: 'Análises', icon: 'fa fa-chart-bar text-primary', class: '', nivel: 'admin' },
  { path: '/analysis-room', title: 'Sala de Análises', icon: 'fa fa-chart-line text-primary', class: '', nivel: 'admin' },
  // 
  // { path: '/profile', title: 'Meu Perfil', icon: 'fa fa-user text-primary', class: '', nivel: 'client' },
  { path: '/analysis-room', title: 'Sala de Análises', icon: 'fa fa-chart-line text-primary', class: '', nivel: 'client' },
  { path: '/analysis-room/subscriptions', title: 'Gerenciar Assinatura', icon: 'fa fa-shopping-cart text-primary', class: '', nivel: 'client' },

];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
  providers: [EvoService]
})
export class SidebarComponent implements OnInit {

  public menuItems: any[];
  public isCollapsed = true;
  public empresa;
  public empresa_padrao;
  public user;
  public empresaId = 1;
  public modules;

  constructor(
    public global: Global,
    public service: EvoService
  ) {
    this.user = this.global.getLoggedUser();
  }

  ngOnInit() {
    this.modules = ROUTES;
    console.log('Modules ->', this.modules);
    console.log(this.global.getLoggedUser());
  }
}
