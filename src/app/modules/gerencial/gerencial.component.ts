import { Component, OnInit } from '@angular/core';
import { Global } from 'src/app/@core/global';

@Component({
  selector: 'app-gerencial',
  templateUrl: './gerencial.component.html',
  styleUrls: ['./gerencial.component.css']
})
export class GerencialComponent implements OnInit {

  public modulo;
  constructor(
    public global: Global
  ) { }

  ngOnInit(): void {
    // this.modulo = this.global.getPermissions('gerencial');
    // console.log('Modulos ->', this.modulo);
  }

}
