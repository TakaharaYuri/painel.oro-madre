import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { EvoService } from 'src/app/@core/evo.service';
import { Global } from 'src/app/@core/global';

@Component({
  selector: 'app-modal-coin-pairs',
  templateUrl: './modal-coin-pairs.component.html',
  styleUrls: ['./modal-coin-pairs.component.css']
})
export class ModalCoinPairsComponent implements OnInit {

  @Input() entity;
  constructor(
    public global: Global,
    public service: EvoService,
    private activeModal: NgbActiveModal,
  ) {
    this.service.entityName = 'coin-pairs';
  }

  ngOnInit(): void {
  }

  close() {
    this.activeModal.close(false);
  }

  save() {
    if (this.entity.id) {
      this.service.updateResource(this.entity).subscribe(response => {
        this.activeModal.close(true);
        this.global.iziToas.success({ title: 'Registro atualizado com sucesso' });
      });
    }
    else {
      this.service.createResource(this.entity).subscribe(response => {
        this.activeModal.close(response);
        this.global.iziToas.success({ title: 'Registro adicionado com sucesso' });
      });
    }
  }

}
