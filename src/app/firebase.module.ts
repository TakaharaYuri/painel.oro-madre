import { NgModule } from '@angular/core';
import { AngularFireModule, FirebaseOptionsToken } from '@angular/fire';
import { AngularFirestoreModule, AngularFirestore } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireAuth } from '@angular/fire/auth';

export function firebaseData() {
    let firebaseConfig = {
        apiKey: "AIzaSyAzQ8x2Yrslk3vVl9yE7pdAnnGmuAM05oc",
        authDomain: "forcadevenda-13558.firebaseapp.com",
        databaseURL: "https://forcadevenda-13558.firebaseio.com",
        projectId: "forcadevenda-13558",
        storageBucket: "forcadevenda-13558.appspot.com",
        messagingSenderId: "329596224097",
        appId: "1:329596224097:web:e24f4e47cb2392ca60a020"
    };
    return firebaseConfig
}

@NgModule({
    imports: [
        AngularFireModule,
        AngularFirestoreModule,
        AngularFireStorageModule
    ],
    declarations: [

    ],
    providers: [
        { provide: FirebaseOptionsToken, useValue: firebaseData() }
    ],
    exports: [
        AngularFireModule,
        AngularFirestoreModule,
        AngularFireStorageModule
    ]
})
export class FirebaseModule {

}

