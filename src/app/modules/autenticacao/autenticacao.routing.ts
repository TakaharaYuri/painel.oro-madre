import { Routes } from '@angular/router';

import { RegistroCadastroComponent } from './registro/registro-cadastro/registro-cadastro.component';
import { PlanSelectComponent } from './registro/plan-select/plan-select.component';
import { PaymentPlanComponent } from './registro/payment-plan/payment-plan.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './forgot-password/reset-password/reset-password.component';

export const AutenticacaoRoutes: Routes = [
    { path: 'register', component: RegistroCadastroComponent },
    { path: 'plan-select', component: PlanSelectComponent },
    { path: 'payment-plan/:payment_method', component: PaymentPlanComponent },
    { path: 'forgot-password', component: ForgotPasswordComponent },
    { path: 'forgot-password/reset/:key', component: ResetPasswordComponent }
];
