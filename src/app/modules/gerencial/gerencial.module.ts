import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GerencialRoutingModule } from './gerencial-routing.module';
import { EmpresaComponent } from './empresa/empresa.component';
import { ModalEmpresaComponent } from './empresa/modal-empresa/modal-empresa.component';
import { FormsModule } from '@angular/forms';
import { MomentModule } from 'ngx-moment';
import { SetorComponent } from './setor/setor.component';
import { ModalSetorComponent } from './setor/modal-setor/modal-setor.component';
import { UsuarioComponent } from './usuario/usuario.component';
import { ModalUsuarioComponent } from './usuario/modal-usuario/modal-usuario.component';
import { NgbTooltipModule, NgbDatepickerModule, NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { YopsilonMaskModule } from 'yopsilon-mask';
import { PerfilComponent } from './usuario/perfil/perfil.component';
import { PipesModule } from 'src/app/@core/pipes/pipes.module';
import { ConfiguracoesComponent } from './configuracoes/configuracoes.component';
import { GerencialComponent } from './gerencial.component';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { UsuarioDetalhesComponent } from './usuario/usuario-detalhes/usuario-detalhes.component';
import { EmpresaDetalhesComponent } from './empresa/empresa-detalhes/empresa-detalhes.component';
import { ComponentsModule } from 'src/app/components/components.module';
import { ModalConviteComponent } from './usuario/modal-convite/modal-convite.component';
import { QrcodesComponent } from './qrcodes/qrcodes.component';
import { ModalQrcodeComponent } from './qrcodes/modal-qrcode/modal-qrcode.component';
import { QrcodeDashboardComponent } from './qrcodes/qrcode-dashboard/qrcode-dashboard.component';
import { IntegracoesComponent } from './integracoes/integracoes.component';

@NgModule({
  imports: [
    CommonModule,
    GerencialRoutingModule,
    FormsModule,
    MomentModule,
    NgbTooltipModule,
    NgbDropdownModule,
    YopsilonMaskModule,
    PipesModule,
    NgxSkeletonLoaderModule,
    NgbDatepickerModule,
    ComponentsModule
  ],
  declarations: [
    EmpresaComponent,
    ModalEmpresaComponent,
    SetorComponent,
    ModalSetorComponent,
    UsuarioComponent,
    ModalUsuarioComponent,
    PerfilComponent,
    ConfiguracoesComponent,
    GerencialComponent,
    UsuarioDetalhesComponent,
    EmpresaDetalhesComponent,
    ModalConviteComponent,
    QrcodesComponent,
    ModalQrcodeComponent,
    QrcodeDashboardComponent,
    IntegracoesComponent,
  ]

})
export class GerencialModule { }
