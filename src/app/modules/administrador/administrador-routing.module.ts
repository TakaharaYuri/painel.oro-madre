import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { AdministradorComponent } from './administrador.component';
import { ModulosComponent } from './modulos/modulos.component';


const routes: Routes = [
  { path: '', component: AdministradorComponent },
  { path: 'modulos', component: ModulosComponent },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    FormsModule
  ],
  exports: [RouterModule]
})
export class AdministradorRoutingModule { }
