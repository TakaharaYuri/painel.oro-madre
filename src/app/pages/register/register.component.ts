import { Component, OnInit } from '@angular/core';
import { Global } from 'src/app/@core/global';
import { EvoService } from 'src/app/@core/evo.service';
import { ActivatedRoute, Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  public key;
  public empresaRef;

  constructor(
    public global: Global,
    public service: EvoService,
    public route: ActivatedRoute,
    public router: Router
  ) {
    this.route.queryParams.subscribe(param => {
      if (param.key && param.ref) {
        this.key = param.key;
        this.empresaRef = param.ref;
      }
      else {
        Swal.fire({
          title: 'Ops!',
          icon: 'warning',
          text: 'O link do seu convite não é válido. Solicite ao seu gerente um novo convite.',
          showCancelButton: true,
          confirmButtonText: 'Solicitar novo Convite',
          cancelButtonText: 'Fechar',
        }).then(result => {
          if (result.value) {

          }
          else {
            this.router.navigate(['login'])
          }
        })
      }
    })
  }

  ngOnInit() {
  }

  getData() {
    // this.service.entityName = 'acoes/get-data'
  }

}
