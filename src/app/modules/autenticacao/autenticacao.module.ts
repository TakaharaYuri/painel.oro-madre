import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RegistroComponent } from './registro/registro.component';
import { RegistroCadastroComponent } from './registro/registro-cadastro/registro-cadastro.component';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { YopsilonMaskModule } from 'yopsilon-mask';
import { NgxMaskModule } from 'ngx-mask'
import { BlockUIModule } from 'ng-block-ui';
import { PlanSelectComponent } from './registro/plan-select/plan-select.component';
import { PaymentPlanComponent } from './registro/payment-plan/payment-plan.component';
import { AutenticacaoRoutes } from './autenticacao.routing';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './forgot-password/reset-password/reset-password.component';
import { ModalPlanCupomComponent } from './registro/plan-select/modal-plan-cupom/modal-plan-cupom.component';
import { NgxUpperCaseDirectiveModule } from 'ngx-upper-case-directive';
 




@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(AutenticacaoRoutes),
        FormsModule,
        RouterModule,
        NgbTooltipModule,
        YopsilonMaskModule,
        NgxMaskModule.forRoot(),
        BlockUIModule.forRoot(),
        NgxUpperCaseDirectiveModule,


    ],
    declarations: [
        RegistroComponent, 
        RegistroCadastroComponent, 
        PlanSelectComponent, 
        PaymentPlanComponent, ForgotPasswordComponent, ResetPasswordComponent, ModalPlanCupomComponent
    ]
})
export class AutenticacaoModule {
    constructor() { }
}
