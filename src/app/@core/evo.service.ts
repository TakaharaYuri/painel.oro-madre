import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
import { HttpClient } from '@angular/common/http';
import swal from 'sweetalert2';
import { Ng2IzitoastService } from 'ng2-izitoast';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EvoService extends BaseService {

  constructor(
    public http: HttpClient,
    private iziToast: Ng2IzitoastService) {
    super(http);
  };


  public save(data = null) {
    if (data.id) {
      return this.updateResource(data)
    }
    else {
      return this.createResource(data);
    }
  }

  public onDeleteConfirm(id=null, text=null) {
    return swal.fire({
      title: 'Excluir registro?',
      html: (!text)? `<h4>Você realmente deseja remover este registro? Esta ação não poderá ser desfeita'</h4>`:`<h4> Você realmente deseja remover este registro? Esta ação não poderá ser desfeita</h4> <small class="text-danger">${text}</small>`,
      confirmButtonText: 'Sim',
      cancelButtonText: 'Não',
      showCancelButton: true,
      icon: 'warning',
    }).then(result => {
      if (result.value) {
        return this.deleteResource(id);
      }
    })
  }

  public onAlertConfirm(data:any, message) {
    return swal.fire({
      title: message.title,
      text: message.text,
      confirmButtonText: 'Sim',
      cancelButtonText: 'Não',
      showCancelButton: true,
      icon: 'warning',
    }).then(result => {
      if (result.value) {
        return data;
      }
    })
  }
}
