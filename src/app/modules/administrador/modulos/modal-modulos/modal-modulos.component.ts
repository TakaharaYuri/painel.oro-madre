import { Component, OnInit, Input } from '@angular/core';
import { Global } from 'src/app/@core/global';
import { EvoService } from 'src/app/@core/evo.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-modal-modulos',
  templateUrl: './modal-modulos.component.html',
  styleUrls: ['./modal-modulos.component.css']
})
export class ModalModulosComponent implements OnInit {

  @Input() entity:any = {};
  modulos;

  constructor(
    public global: Global,
    public service: EvoService,
    private activeModal: NgbActiveModal,
  ) { }

  ngOnInit(): void {
    this.getModules();
  }

  save() {
    this.service.save(this.entity).subscribe(response => {
      this.global.iziToas.success({ title: 'Módulo cadastrado com Sucesso' });
      this.activeModal.close(true);
    },
      error => {
        this.global.iziToas.error({ title: 'Ocorreu um problema ao processar sua requisição' });
      })
  }

  close() {
    this.activeModal.close(false);
  }

  getModules() {
    this.service.getResources().subscribe(response => {
      this.modulos = response;
    })
  }

}
