import { Component, OnInit } from '@angular/core';
import { Global } from 'src/app/@core/global';
import { EvoService } from 'src/app/@core/evo.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalModulosComponent } from './modal-modulos/modal-modulos.component';

@Component({
  selector: 'app-modulos',
  templateUrl: './modulos.component.html',
  styleUrls: ['./modulos.component.css']
})
export class ModulosComponent implements OnInit {

  public data;

  constructor(
    public global: Global,
    public service: EvoService,
    private modalService: NgbModal
  ) {
    this.service.entityName = 'modulos';
  }

  ngOnInit(): void {
    this.get();
  }

  get() {
    this.service.getResources().subscribe(response => {
      this.data = response;
      console.log(response);
    })
  }

  open(data = {}) {
    const modal = this.modalService.open(ModalModulosComponent, {
      size: 'md',
      keyboard: false,
      backdrop: 'static'
    });

    modal.componentInstance.entity = data;

    modal.result.then((result) => {
      if (result) {
        this.get();
      }
    });
  }

  delete(id) {
    this.service.onDeleteConfirm(id).then(confirm => {
      if (confirm) {
        confirm.subscribe(response => {
          this.global.iziToas.success({ title: 'Registro removido com sucesso' });
          this.get();
        })
      }
    })
  }

  updateStatus(item) {
    if (item.status == 1) item.status = 0;
    else if (item.status == 0) item.status = 1;

    if (item.ativo == 1) item.ativo = 0;
    if (item.ativo == 0) item.ativo = 1;
    this.service.updateResource(item).subscribe(response => {
      console.log('response =>', response);
      this.global.iziToas.success({ title: 'Produto atualizado com sucesso' });
      this.get();
    })
  }

}
