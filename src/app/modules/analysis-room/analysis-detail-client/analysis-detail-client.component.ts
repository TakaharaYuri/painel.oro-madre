import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { empty } from 'rxjs';
import { EvoService } from 'src/app/@core/evo.service';
import { Global } from 'src/app/@core/global';

@Component({
  selector: 'app-analysis-detail-client',
  templateUrl: './analysis-detail-client.component.html',
  styleUrls: ['./analysis-detail-client.component.css']
})
export class AnalysisDetailClientComponent implements OnInit {

  public entity: any = {}
  public onError = false;
  constructor(
    public global: Global,
    public service: EvoService,
    public router: ActivatedRoute
  ) {
    this.router.params.subscribe(params => {
      if (params.id) {
        this.get(params.id)
      }
      else {

      }
    })
  }

  ngOnInit(): void {
  }

  get(id) {
    this.service.entityName = `analysis-room/detail/${id}`;
    this.service.getResources().subscribe((response: any) => {
      if (response.result == true) {
        this.entity = response.data;
      }
      else {
        this.onError = true;
      }
    })
  }

  sendLike(entity) {
    this.service.entityName = 'analysis-room/likes';
    if (!entity.is_liked) {
      const normalization = {
        user_id: this.global.getLoggedUser().id,
        analysis_id: entity.id
      }
      this.service.createResource(normalization).subscribe((response: any) => {
        if (response.result === true) {
          entity.likes_count++;
          entity.is_liked = true;
          entity.like_id = response.data.id;
        }
      })
    }
    else {
      this.service.deleteResource(entity.like_id).subscribe((response: any) => {
        if (response.result === true) {
          entity.likes_count--;
          entity.is_liked = false;
          entity.like_id = null;
        }
      })
    }
  }

}
