import { Component, OnInit } from '@angular/core';
import { EvoService } from 'src/app/@core/evo.service';
import { Global } from 'src/app/@core/global';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-subscriptions',
  templateUrl: './subscriptions.component.html',
  styleUrls: ['./subscriptions.component.css']
})
export class SubscriptionsComponent implements OnInit {

  public entity: any;

  constructor(
    public global: Global,
    public service: EvoService
  ) { }

  ngOnInit(): void {
    this.get();
  }

  get() {
    this.service.entityName = 'pagarme/subscription';
    this.service.getResources().subscribe(response => {
      this.entity = response;
    })
  }

  cancelSubscription() {
    Swal.fire({
      title: 'Você realmente deseja cancelar sua assinatura na Plataforma OroMadre?',
      html: `Caso você opte por cancelar sua assinatura e seu periodo de testes ainda esteja ativo, você poderá utilizar a plataforma enquanto o periodo estiver em vigor, caso o contrário seu acesso será bloqueado assim que o seu plano atual chegar ao fim.`,
      confirmButtonText: 'Sim',
      cancelButtonText: 'Não',
      showCancelButton: true,
      icon: 'warning',
    }).then(result => {
      if (result.value) {
        this.service.entityName = 'pagarme/cancel-subscription';
        this.service.getResources().subscribe((response: any) => {
          if (response.result == true) {
            this.global.logout();
          }
        });
      }
    })
  }

}
