import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, LOCALE_ID, enableProdMode } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import { AuthLayoutComponent } from './layouts/auth-layout/auth-layout.component';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app.routing';
import { ComponentsModule } from './components/components.module';
import { Ng2IziToastModule } from 'ng2-izitoast';
import { MomentModule } from 'ngx-moment';
import { Global } from './@core/global';
import { EvoService } from './@core/evo.service';
import { HttpInterceptorService } from './@core/http.interceptor';
import { InternationalizationService } from 'yopsilon-mask';
import { registerLocaleData } from '@angular/common';
import localePt from '@angular/common/locales/pt';
import * as moment from 'moment';
import { environment } from 'src/environments/environment';
import { AutenticacaoModule } from './modules/autenticacao/autenticacao.module';

// import * as locale from 'moment';
moment.locale('pt-BR');


registerLocaleData(localePt, 'pt-BR');

//I keep the new line
@NgModule({
  imports: [
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    ComponentsModule,
    NgbModule,
    RouterModule,
    AppRoutingModule,
    Ng2IziToastModule,
    MomentModule,
    AutenticacaoModule
  ],
  declarations: [
    AppComponent,
    AdminLayoutComponent,
    AuthLayoutComponent,
  ],
  exports: [
    MomentModule
  ],
  providers: [
    InternationalizationService,
    Global,
    EvoService,
    { provide: LOCALE_ID, useValue: 'pt-BR' },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpInterceptorService,
      multi: true,
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor() { 
    if (environment.production) {
      window.console.log = function () { };
    }
  }
}
