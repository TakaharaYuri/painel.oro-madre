import { Component, OnInit } from '@angular/core';
import { EvoService } from 'src/app/@core/evo.service';
import { MatDialog } from '@angular/material';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalEmpresaComponent } from './modal-empresa/modal-empresa.component';
import { newArray } from '@angular/compiler/src/util';
import { Global } from 'src/app/@core/global';
import { environment } from  'src/environments/environment';

@Component({
  selector: 'app-empresa',
  templateUrl: './empresa.component.html',
  styleUrls: ['./empresa.component.css'],
  providers: [EvoService]
})
export class EmpresaComponent implements OnInit {

  public data;
  public page;
  public actualPage = 1;
  public lastPage;
  public totalPages;
  public apiUrl;
  public search = {};

  constructor(
    public service: EvoService,
    private modalService: NgbModal,
    public global: Global
  ) {
      
  }

  ngOnInit(): void {
    this.get();
    this.apiUrl = environment.host;
  }

  get() {
    this.service.entityName = `empresa?page=${this.actualPage}`;
    this.service.getResources().subscribe((response:any) => {
      this.data = response.data;
      this.lastPage = response.lastPage;
      this.totalPages = newArray(this.lastPage);
      console.log(this.lastPage);
    });
  }

  getPaginate(page) {
    this.actualPage = page;
    this.service.entityName = `empresa?page=${this.actualPage}&search=${this.search}`;
    this.service.getResources().subscribe((response:any) => {
      this.data = response.data;
      this.lastPage = response.lastPage;
      console.log(this.actualPage);
      console.log(this.lastPage);
    });
  }

  open(data = {}) {
    const modal = this.modalService.open(ModalEmpresaComponent, {
      size: 'lg',
      keyboard: false,
      backdrop: 'static',
    });
    modal.componentInstance.entity = data;
    console.log('Data ->', data);

    modal.result.then((result) => {
      if (result) {
        this.get();
      }
    });
  }

  delete(id) {
    this.service.entityName = 'empresa';
    this.service.onDeleteConfirm(id, 'Você não terá mais acesso aos dados ligados a esta empresa').then(confirm => {
      if (confirm) {
        confirm.subscribe(response => {
          this.global.iziToas.success({ title: 'Registro removido com sucesso' });
          this.get();
        })
      }
    })
  }

  generateQRCode(id) {
    this.service.entityName = `empresa/gerenerate_qrcode/${id}`;
    const message = {
      title:'Tem certeza?',
      text:'Ao gerar um novo QRCode o antigo se tornará inválido e será removido do sistema'
    }
    this.service.onAlertConfirm(this.service.getResources(), message).then(confirm => {
      if (confirm) {
        confirm.subscribe( response => {
          console.log('onAlertConfirm', response);
          this.global.iziToas.success({title:'QRCode gerado com sucesso', message:'Lembre-se que o QRCode antigo deixa de ter válidade'})
          this.get();
        })
      }
    })
  }

}
