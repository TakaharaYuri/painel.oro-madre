import { Component, OnInit } from '@angular/core';
import { EvoService } from 'src/app/@core/evo.service';
import { Global } from 'src/app/@core/global';
import { HttpEventType, HttpErrorResponse } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';
import { of } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-empresa-detalhes',
  templateUrl: './empresa-detalhes.component.html',
  styleUrls: ['./empresa-detalhes.component.css']
})
export class EmpresaDetalhesComponent implements OnInit {

  public id;

  constructor(
    private global: Global,
    private service: EvoService,
    public route: ActivatedRoute,
    public router:Router
  ) { 
    this.route.params.subscribe(param => {
      if (param.id) {
        this.id = param.id;
      }
    });
  }

  public entity: any = {};
  public img;

  ngOnInit(): void {
    if (this.id) this.get();
  }

  public get() {
    this.service.entityName = 'empresa';
    this.service.getResource(this.id).subscribe(response => {
      this.entity = response;
    })
  }

  save() {
    this.service.entityName = 'empresa';
    this.service.save(this.entity).subscribe(
      response => {
        this.router.navigate(['gerencial/empresas']);
        this.global.iziToas.success({title:'Dados salvos com sucesso'});
      },
      error => {
        this.global.iziToas.error({ title: 'Ocorreu um problema ao processar sua solicitação' });
      })
  }

  loadImage(image) {
    let tmpFile = image.target.files.item(0)
    this.uploadImage(tmpFile);

    if (FileReader) {
      var fr = new FileReader();
      fr.onload = () => {
        this.img = fr.result;
      }

      fr.readAsDataURL(tmpFile);
    }

    else {
      this.global.iziToas.warning({ title: 'Metodo não compátivel com sua versão' });
    }
  }

  uploadImage(file) {
    const formData = new FormData();
    formData.append('image', file);
    file.inProgress = true;
    this.service.upload(formData).pipe(
      map(event => {
        switch (event.type) {
          case HttpEventType.UploadProgress:
            file.progress = Math.round(event.loaded * 100 / event.total);
            console.log('file', file);
            break;
          case HttpEventType.Response:
            console.log('event', event);
            return event;
        }
      }),
      catchError((error: HttpErrorResponse) => {
        file.inProgress = false;
        return of(`${file.name} upload failed.`);
      })).subscribe((event: any) => {
        if (typeof (event) === 'object') {
          this.entity.logo = event.body.fileName;
        }
      });
  }

  generateIdentifier(text) {
    text = text.replace(/[^\w\s]/gi, '');
    text = text.split('  ').join(' ');
    text = text.split('-').join('');
    text = text.split(' ').join('-');
    text = text.split('--').join('-');
    text = text.toLowerCase();
    console.log(text);
    this.entity.identificador = text.split(' ').join('-');
  }

}
