import { Component, OnInit } from '@angular/core';
import { EvoService } from 'src/app/@core/evo.service';
import { Global } from 'src/app/@core/global';

@Component({
  selector: 'app-analysis-manager',
  templateUrl: './analysis-manager.component.html',
  styleUrls: ['./analysis-manager.component.css']
})
export class AnalysisManagerComponent implements OnInit {

  public data;
  public filter: any = {};
  public isFiltered = false;
  public users;
  public coinPairs;
  public loading = false;

  constructor(
    public service: EvoService,
    public global: Global
  ) { 
    this.global.adminPermission();
  }

  ngOnInit(): void {
    this.getCoinPairs();
    this.getUsers();
    this.get();
  }

  get(search = '') {
    this.isFiltered = (search != '') ? true : false;

    this.loading = true;
    this.service.entityName = `analysis-manager?${this.global.serialize(search)}`;
    this.service.getResources().subscribe(response => {
      this.data = response;
      this.loading = false;
    })
  }

  getUsers() {
    this.service.entityName = 'users';
    this.service.getResources().subscribe(response => {
      this.users = response;
    })
  }

  getCoinPairs() {
    this.service.entityName = 'coin-pairs';
    this.service.getResources().subscribe((response: any) => {
      this.coinPairs = response;
    })
  }

  delete(id) {
    this.service.entityName = `analysis-manager`;
    this.service.onDeleteConfirm(id).then(confirm => {
      if (confirm) {
        confirm.subscribe(response => {
          this.global.iziToas.success({ title: 'Registro removido com sucesso' });
          this.get();
        })
      }
    })
  }

  clear() {
    this.isFiltered = false;
    this.filter = {};
  }

}
