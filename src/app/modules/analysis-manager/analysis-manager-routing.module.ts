import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AnalysisDetailsComponent } from './analysis-details/analysis-details.component';
import { AnalysisManagerComponent } from './analysis-manager.component';
import { CoinPairsComponent } from './coin-pairs/coin-pairs.component';
import { CuponsComponent } from './cupons/cupons.component';
import { PlansComponent } from './plans/plans.component';


const routes: Routes = [
  {
    path: '',
    component: AnalysisManagerComponent
  },
  {
    path: 'details',
    component: AnalysisDetailsComponent
  },
  {
    path: 'details/:id',
    component: AnalysisDetailsComponent
  },
  {
    path: 'coin-pairs',
    component: CoinPairsComponent
  },
  {
    path: 'plans',
    component: PlansComponent
  },
  {
    path: 'cupons',
    component: CuponsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AnalysisManagerRoutingModule { }
