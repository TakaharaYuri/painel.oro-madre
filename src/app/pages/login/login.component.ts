import { Component, OnInit, OnDestroy } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Global } from 'src/app/@core/global';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers: [Global]
})
export class LoginComponent implements OnInit, OnDestroy {

  public loading = false;
  public host = environment.apiUrl
  public email;
  public password;

  constructor(
    public http: HttpClient,
    public global: Global,
    public route: Router
  ) { }

  ngOnInit() {
  }
  ngOnDestroy() {
  }

  public login() {
    this.loading = true;
    let data = {
      email: this.email,
      password: this.password
    }

    this.http.post(`${this.host}/login`, data).subscribe(
      ((response: any) => {
        this.loading = false;
        if (response.result) {
          this.global.iziToas.success({
            title: 'Pronto!',
            message: 'Login realizado com sucesso'
          });

          localStorage.setItem('token', response.token.token);
          localStorage.setItem('user', JSON.stringify(response.user));

          const user = this.global.getLoggedUser();
          if (user.type === 'admin') {
            this.route.navigate(['/dashboard']);
          }
          else {
            this.route.navigate(['/analysis-room']);
          }


          console.log('Response ->', response);
        }
        else {
          this.global.iziToas.warning({
            title: 'Ops!',
            message: response.message
          })
        }
      }),
      ((error) => {
        this.loading = false;
        this.global.iziToas.error({
          title: 'Ops',
          message: 'Email ou senha inválidos, verifique e tente novamente.',
          maxWidth: '300px'
        });
      })
    )
  };

}
