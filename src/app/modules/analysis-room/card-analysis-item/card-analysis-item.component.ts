import { Component, Input, OnInit } from '@angular/core';
import { Global } from 'src/app/@core/global';

@Component({
  selector: 'app-card-analysis-item',
  templateUrl: './card-analysis-item.component.html',
  styleUrls: ['./card-analysis-item.component.css']
})
export class CardAnalysisItemComponent implements OnInit {

  @Input() item;
  constructor(
    public global: Global
  ) { }

  ngOnInit(): void {
  }

}
