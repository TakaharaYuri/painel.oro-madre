import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EvoService } from 'src/app/@core/evo.service';
import { Global } from 'src/app/@core/global';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {

  public entity: any = {};
  public passwordConfirmation;
  public key;
  constructor(
    public global: Global,
    public service: EvoService,
    public route: ActivatedRoute,
    public router: Router
  ) {
    this.route.params.subscribe(params => {
      this.key = params.key;
      console.log('KEY ->', this.key);
      this.get();
    })
  }

  ngOnInit(): void {
  }
  get() {
    this.service.entityName = `reset-password-validate/${this.key}`;
    this.service.getResources().subscribe(
      (response: any) => {
        if (response.result == true) {
          this.entity.key = response.key;
        }
        else {
          Swal.fire({
            title: 'Ops!',
            icon: 'warning',
            text: response.message,
            confirmButtonText: 'Voltar',
          }).then(result => {
            this.router.navigate(['/'])
          })
        }
      },
      (error => {
        Swal.fire({
          title: 'Ops!',
          icon: 'warning',
          text: 'Ocorreu um erro ao processar sua requisição',
          confirmButtonText: 'Voltar',
        }).then(result => {
          this.router.navigate(['/'])
        })
      }))
  }

  save() {
    this.service.entityName = 'create-password';
    this.service.createResource(this.entity).subscribe((response: any) => {
      if (response.result == true) {
        Swal.fire({
          title: 'Pronto!',
          icon: 'success',
          text: response.message,
          confirmButtonText: 'Certo!',
        }).then(result => {
          this.router.navigate(['/'])
        })
      }
      else {
        this.global.iziToas.show({
          title: 'Ops',
          message: 'Ocorreu um problema ao salvar sua senha, tente novamente mais tarde'
        })
      }
    });
  }

}
