import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { EvoService } from 'src/app/@core/evo.service';
import { Global } from 'src/app/@core/global';

@Component({
  selector: 'app-modal-plan-cupom',
  templateUrl: './modal-plan-cupom.component.html',
  styleUrls: ['./modal-plan-cupom.component.css']
})
export class ModalPlanCupomComponent implements OnInit {


  public entity:any = {};
  public data;

  constructor(
    public service: EvoService,
    public global: Global,
    public activeModal: NgbActiveModal
  ) { }

  ngOnInit(): void {
  }

  get() {
    this.global.loading = true;
    this.service.entityName = `validate-cupom/${this.entity.cupom}`;
    this.service.getResources().subscribe((response: any) => {
      console.log('Response', response);
      this.data = response;

      if (response.result === true) {
        this.activeModal.close(response.data);
      }

      this.global.loading = false;

    });
  }

  close() {
    this.activeModal.close(false);
  }

}
