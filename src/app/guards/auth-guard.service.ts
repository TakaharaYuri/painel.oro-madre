import { Injectable } from '@angular/core';
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivate } from '@angular/router';
import { Global } from '../@core/global';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor(
    private router: Router,
    private global: Global,
    ) { }

  canActivate(route:ActivatedRouteSnapshot, state:RouterStateSnapshot):boolean {
    let url = state.url;
    return this.verifyLogin(url);
  }

  private verifyLogin(url) {
    if (!this.isLogged()) {
      this.router.navigate(['/login']);
      return false;
    }
    else if (this.isLogged()) {
      return true;
    }
  }

  public isLogged(): boolean {
    console.log(this.global.getPermissions());
    if (localStorage.getItem('user') && localStorage.getItem('token')) {
      return true;
    }
    else {
      return false;
    }
  }
}
