import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { EvoService } from 'src/app/@core/evo.service';
import { Global } from 'src/app/@core/global';

@Component({
    selector: 'app-clientes',
    templateUrl: './clientes.component.html',
    styleUrls: ['./clientes.component.css']
})
export class ClientesComponent implements OnInit {
    public filter: any = {};
    public data;
    public isFiltered = true;

    constructor(
        public global: Global,
        public service: EvoService,
    ) {
        this.global.adminPermission();
    }

    ngOnInit(): void {
        this.get();
    }

    get() {
        // console.log(this.filter == {});
        this.service.entityName = `clients?${this.global.serialize(this.filter)}`;
        this.service.getResources().subscribe((response: any) => {
            this.data = response.data
        })
    }

    delete(id) {
        this.service.entityName = `clients`;
        this.service.onDeleteConfirm(id).then(confirm => {
            if (confirm) {
                confirm.subscribe(response => {
                    this.global.iziToas.success({ title: 'Registro removido com sucesso' });
                    this.get();
                })
            }
        })
    }

    save() {
        this.service.getResources().subscribe(
            (response: any) => {
                if (response.result == true) {
                    //response.data
                }
                else {
                    //response.result == error
                }
            },
            (error) => {
                //response error
            }
        );
    }

    open(data = {}) {

    }

    clear() {
        this.filter = {};
    }

    // open(data = {}) {
    //   const modal = this.modalService.open(ModalChamadosComponent, {
    //     size: 'lg',
    //     keyboard: false,
    //     backdrop: 'static'
    //   });

    //   modal.componentInstance.data = data;

    //   modal.result.then((result) => {
    //     if (result) {
    //       // this.get();
    //     }
    //   });
    // }

}
