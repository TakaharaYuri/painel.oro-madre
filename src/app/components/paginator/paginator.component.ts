import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-paginator',
  templateUrl: './paginator.component.html',
  styleUrls: ['./paginator.component.css']
})
export class PaginatorComponent implements OnInit {

  @Input() pagination;
  @Input() paginate = true;
  @Output() changePage = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  public doPageChange(page) {
    let max = this.pagination.last_page;
    if ((page >= 1) && (page <= max)) {
      this.changePage.emit(page);
    }
  }

}
