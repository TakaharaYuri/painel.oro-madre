import { Component, OnInit } from '@angular/core';
import { EvoService } from 'src/app/@core/evo.service';
import { Global } from 'src/app/@core/global';

@Component({
  selector: 'app-card-user-profile',
  templateUrl: './card-user-profile.component.html',
  styleUrls: ['./card-user-profile.component.css']
})
export class CardUserProfileComponent implements OnInit {

  public entity: any;
  constructor(
    public global: Global,
    public service: EvoService
  ) { }

  ngOnInit(): void {
    this.get();
  }

  get(){
    this.service.entityName = 'logged-user';
    this.service.getResources().subscribe(response => {
      this.entity = response;
    })
  }

}
