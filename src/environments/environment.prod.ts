export const environment = {
  production: true,
  apiUrl:'https://api-oromadre.herokuapp.com/api',
  host: 'https://api-oromadre.herokuapp.com'
  // apiUrl:'https://carbonor-api.herokuapp.com/api',
  // host: 'https://carbonor-api.herokuapp.com'
};
