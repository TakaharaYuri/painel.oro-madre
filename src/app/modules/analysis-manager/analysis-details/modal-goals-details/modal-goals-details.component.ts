import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-modal-goals-details',
  templateUrl: './modal-goals-details.component.html',
  styleUrls: ['./modal-goals-details.component.css']
})
export class ModalGoalsDetailsComponent implements OnInit {

  @Input() entity: any;
  constructor(
    public activeModal: NgbActiveModal
  ) { }

  ngOnInit(): void {
  }

  save() {
    this.activeModal.close(this.entity);
  }

  close() {
    this.activeModal.close(false);
  }

}
