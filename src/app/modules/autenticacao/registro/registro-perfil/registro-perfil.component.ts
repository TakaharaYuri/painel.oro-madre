import { Component, OnInit } from '@angular/core';
import { Global } from 'src/app/@core/global';
import { EvoService } from 'src/app/@core/evo.service';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpEventType, HttpErrorResponse } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';
import { of } from 'rxjs';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-registro-perfil',
  templateUrl: './registro-perfil.component.html',
  styleUrls: ['./registro-perfil.component.css']
})
export class RegistroPerfilComponent implements OnInit {

  public img;
  public entity: any = {};
  public loading = false;
  public key;
  public empresaRef;
  constructor(
    public global: Global,
    public service: EvoService,
    public route: ActivatedRoute,
    public router: Router
  ) {
    this.route.queryParams.subscribe(param => {
      if (param.key) {
        this.key = param.key;
        this.empresaRef = param.ref;
        let _session = JSON.parse(localStorage.getItem('newUser'));
        if (_session) {
          this.entity = _session.usuario;
          console.log(_session);
        }
        else {
          this.onValideError();
        }

      }
      else {
        this.onValideError();
      }
    });
  }

  ngOnInit(): void {
  }

  loadImage(image) {
    let tmpFile = image.target.files.item(0)
    this.uploadImage(tmpFile);

    if (FileReader) {
      var fr = new FileReader();
      fr.onload = () => {
        this.img = fr.result;
      }

      fr.readAsDataURL(tmpFile);
    }

    else {
      this.global.iziToas.warning({ title: 'Metodo não compátivel com sua versão' });
    }
  }

  save() {
    this.loading = true;
    this.service.entityName = `convite/finalizar`;
    this.service.createResource(this.entity).subscribe(
      response => {
        // console.log('Response ->', response);
        this.router.navigate(['login']);
        this.global.iziToas.show({ title: 'Pronto! Seu cadastro foi realizado com sucesso, faça login para continuar utilizando o sistema' });
      },
      error => {
        // console.log('ResponseError ->', error);
      });
  }

  uploadImage(file) {
    const formData = new FormData();
    formData.append('image', file);
    file.inProgress = true;
    this.service.upload(formData).pipe(
      map(event => {
        switch (event.type) {
          case HttpEventType.UploadProgress:
            file.progress = Math.round(event.loaded * 100 / event.total);
            console.log('file', file);
            break;
          case HttpEventType.Response:
            console.log('event', event);
            return event;
        }
      }),
      catchError((error: HttpErrorResponse) => {
        file.inProgress = false;
        return of(`${file.name} upload failed.`);
      })).subscribe((event: any) => {
        if (typeof (event) === 'object') {
          this.entity.imagem = event.body.fileName;
        }
      });
  }

  onValideError() {
    Swal.fire({
      title: 'Ops!',
      icon: 'warning',
      text: 'O link do seu convite não é válido. Solicite ao seu gerente um novo convite.',
      showCancelButton: true,
      confirmButtonText: 'Solicitar novo Convite',
      cancelButtonText: 'Fechar',
    }).then(result => {
      if (!result.value) {
        this.router.navigate(['login'])
      }
    })
  }

}
