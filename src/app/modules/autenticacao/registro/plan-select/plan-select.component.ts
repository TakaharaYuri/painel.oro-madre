import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { EvoService } from 'src/app/@core/evo.service';
import { Global } from 'src/app/@core/global';
import Swal from 'sweetalert2';
import { ModalPlanCupomComponent } from './modal-plan-cupom/modal-plan-cupom.component';

@Component({
  selector: 'app-plan-select',
  templateUrl: './plan-select.component.html',
  styleUrls: ['./plan-select.component.css']
})
export class PlanSelectComponent implements OnInit {

  public entity: any = {};
  public plan;

  constructor(
    public global: Global,
    public router: Router,
    public service: EvoService,
    public modalService: NgbModal
  ) {
    if (sessionStorage.getItem('customer')) {
      this.entity = JSON.parse(sessionStorage.getItem('customer'));
    }
    else {
      Swal.fire({
        title: 'Ops!',
        icon: 'warning',
        text: 'Você ainda não completou os seus dados cadstrais, volte e tente novamente.',
        confirmButtonText: 'Voltar',
      }).then(result => {
        if (!result.value) {
          this.router.navigate(['/register'])
        }
      })
    }
  }


  getPlan() {
    this.service.entityName = 'active_plan';
    this.service.getResources().subscribe(response => {
      this.plan = response;
      sessionStorage.setItem('plan', JSON.stringify(this.plan));
    })
  }

  ngOnInit(): void {
    this.getPlan();
  }

  openModalPlanCupom(data = {}) {
    const modal = this.modalService.open(ModalPlanCupomComponent, {
      size: 'sm',
      keyboard: false,
      backdrop: 'static'
    });

    modal.result.then((result) => {
      if (result) {
        this.plan = result;
        sessionStorage.setItem('plan', JSON.stringify(this.plan));
        this.entity.plan_id = this.plan.id;
        this.global.iziToas.success({
          title:'Pronto',
          message:'Seu cupom de desconto foi aplicado com sucesso'
        })
      }
    });
  }

}
