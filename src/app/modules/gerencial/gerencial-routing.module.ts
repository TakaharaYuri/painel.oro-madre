import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsuarioComponent } from './usuario/usuario.component';
import { PerfilComponent } from './usuario/perfil/perfil.component';
import { GerencialComponent } from './gerencial.component';
import { UsuarioDetalhesComponent } from './usuario/usuario-detalhes/usuario-detalhes.component';



const routes: Routes = [
  { path: '', component: GerencialComponent },

  { path: 'usuarios', component: UsuarioComponent },
  { path: 'usuarios/visualizar/:id', component: UsuarioDetalhesComponent },
  { path: 'usuarios/novo', component: UsuarioDetalhesComponent },
  { path: 'perfil', component: PerfilComponent },
  
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]   
})
export class GerencialRoutingModule { }


