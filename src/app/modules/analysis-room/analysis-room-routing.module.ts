import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AnalysisDetailClientComponent } from './analysis-detail-client/analysis-detail-client.component';
import { AnalysisRoomComponent } from './analysis-room.component';
import { SubscriptionsComponent } from './subscriptions/subscriptions.component';


const routes: Routes = [
  {
    path: '',
    component: AnalysisRoomComponent
  },
  {
    path: 'detail/:id',
    component: AnalysisDetailClientComponent
  },
  {
    path: 'subscriptions',
    component: SubscriptionsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AnalysisRoomRoutingModule { }
