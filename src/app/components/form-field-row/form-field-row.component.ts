import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'form-field-row',
  templateUrl: './form-field-row.component.html',
  styleUrls: ['./form-field-row.component.css']
})
export class FormFieldRowComponent implements OnInit {

  @Input() labelSize = '3';
  @Input() inputSize = '9';
  @Input() label = 'Campo';
  @Input() help;
  @Input() customClass;
  constructor() { }

  ngOnInit(): void {
  }

}
