import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-page-card',
  templateUrl: './page-card.component.html',
  styleUrls: ['./page-card.component.css']
})
export class PageCardComponent implements OnInit {

  @Input() customClass = 'col-12';
  constructor() { }

  ngOnInit(): void {
  }

}
