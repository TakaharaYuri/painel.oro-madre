import { Component, HostListener } from '@angular/core';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  //Removendo a função de clicar com o botão direito do mouse
  @HostListener('contextmenu', ['$event'])
  onRightClick(event) {
    console.log('Event ->', event);
    if (environment.production) {
      event.preventDefault();
    }
  }

  @HostListener('document:keyup', ['$event'])
  onKeyUp(event: KeyboardEvent) {
    if (event.key == '44' || event.key == '123') {
      event.preventDefault()
      this.stopPrntScr();
    }
  }

  stopPrntScr() {

    var inpFld: any = document.createElement("input");
    inpFld.setAttribute("value", ".");
    inpFld.setAttribute("width", "0");
    inpFld.style.height = "0px";
    inpFld.style.width = "0px";
    inpFld.style.border = "0px";
    document.body.appendChild(inpFld);
    inpFld.select();
    document.execCommand("copy");
    inpFld.remove(inpFld);
    console.log('STOP');
  }

  AccessClipboardData() {
    try {

    } catch (err) {
    }
  }

  title = 'argon-dashboard-angular';
}
