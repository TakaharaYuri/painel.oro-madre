import { Injectable } from '@angular/core';
import {
  HttpInterceptor,
  HttpRequest,
  HttpResponse,
  HttpHandler,
  HttpEvent,
  HttpErrorResponse
} from '@angular/common/http';

import { Observable, throwError, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { Global } from './global';

@Injectable({
  providedIn: 'root'
})
export class HttpInterceptorService implements HttpInterceptor {

  constructor(private router: Router, public global: Global) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    if (localStorage.getItem('token')) {
      if (this.global.useToken) {
        request = request.clone({
          setHeaders: {
            Authorization: 'bearer ' + localStorage.getItem('token')
          }
        });
      }
    }

    /**
     * continues request execution
     */
    return next.handle(request).pipe(catchError((error, caught) => {
      //intercept the respons error and displace it to the console
      console.log(error);
      this.handleAuthError(error);
      return of(error);
    }) as any);
  }


  /**
   * manage errors
   * @param err
   * @returns {any}
   */
  private handleAuthError(err: HttpErrorResponse): Observable<any> {
    if (err.status === 401) {
      localStorage.clear();

      this.router.navigate([`/login`]);
      if (err.error.error.name === 'InvalidJwtToken') {
        this.global.iziToas.error({ title: 'Sessão encerrada, faça login novamente.' });
      }
      else {
        console.log('handled error ' + err.status);
        this.global.iziToas.error({ title: 'Ops!', message: 'Login ou senha inválidos, verifique os dados e tente novamente.', maxWidth: '300px' })
      }
      // if you've caught / handled the error, you don't want to rethrow it unless you also want downstream consumers to have to handle it as well.
      return of(err.message);
    }
    else if (err.status === 400 || err.status === 500 || err.status === 404) {
      this.global.iziToas.error({ title: 'Ops! [400]', message: 'Tivemos um problema ao processar sua requisição, tente novamente mais tarde.', maxWidth: '300px' })
    }
    else {
      localStorage.clear();
      // this.router.navigate([`/login`]);
      this.global.iziToas.error({
        title: 'Ops!',
        message: (err.error.msg) ? err.error.msg : 'Tivemos um problema ao processar sua requisição, tente novamente mais tarde.',
        maxWidth: '300px'
      })
    }
    throw err;
  }
}
