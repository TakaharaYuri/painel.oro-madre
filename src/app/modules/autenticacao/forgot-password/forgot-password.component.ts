import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EvoService } from 'src/app/@core/evo.service';
import { Global } from 'src/app/@core/global';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {

  public entity: any = {};
  constructor(
    public global: Global,
    public service: EvoService,
    public router: Router
  ) { }

  ngOnInit(): void {
  }

  save() {
    this.global.loading = true;
    this.service.entityName = 'reset-password';
    this.service.createResource(this.entity).subscribe(
      (response: any) => {
      if (response.result == true) {
        Swal.fire({
          title: 'Pronto!',
          icon: 'success',
          text: response.message,
          confirmButtonText: 'Voltar',
        }).then(result => {
          this.router.navigate(['/'])
        })
      }
      else {
        this.global.iziToas.show({
          title:'Ops',
          message:response.message
        })
      }
      this.global.loading = false;
    },
    (error) => {
      this.global.loading = false;
      this.global.iziToas.show({
        title:'Ops',
        message:'Ocorreu um problema ao processar sua requisição'
      })
    });
  }

}
