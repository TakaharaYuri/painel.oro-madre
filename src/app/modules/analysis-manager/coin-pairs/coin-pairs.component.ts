import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { EvoService } from 'src/app/@core/evo.service';
import { Global } from 'src/app/@core/global';
import { ModalCoinPairsComponent } from './modal-coin-pairs/modal-coin-pairs.component';

@Component({
  selector: 'app-coin-pairs',
  templateUrl: './coin-pairs.component.html',
  styleUrls: ['./coin-pairs.component.css']
})
export class CoinPairsComponent implements OnInit {

  public data;
  constructor(
    public global: Global,
    public service: EvoService,
    private modalService: NgbModal
  ) {
    this.service.entityName = 'coin-pairs';
  }

  ngOnInit(): void {
    this.get();
  }

  get() {
    this.service.getResources().subscribe(response => {
      this.data = response;
    })
  }

  delete(id) {
    this.service.onDeleteConfirm(id).then(confirm => {
      if (confirm) {
        confirm.subscribe(response => {
          this.global.iziToas.success({ title: 'Registro removido com sucesso' });
          this.get();
        })
      }
    })
  }

  open(data = {}) {
    const modal = this.modalService.open(ModalCoinPairsComponent, {
      size: 'sm',
      keyboard: false,
      backdrop: 'static'
    });

    modal.componentInstance.entity = data;

    modal.result.then((result) => {
      if (result) {
        this.get();
      }
    });
  }

  updateStatus(item) {
    if (item.status == 1) {
      item.status = 0;
    }
    else {
      item.status = 1;
    };
    this.service.updateResource(item).subscribe(response => {
      this.global.iziToas.success({
        title: 'Pronto',
        message: 'Status atualizado com sucesso'
      });
    })
  }

}
