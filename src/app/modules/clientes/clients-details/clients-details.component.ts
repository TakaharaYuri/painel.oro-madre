import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { EvoService } from 'src/app/@core/evo.service';
import { Global } from 'src/app/@core/global';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-clients-details',
  templateUrl: './clients-details.component.html',
  styleUrls: ['./clients-details.component.css']
})
export class ClientsDetailsComponent implements OnInit {

  public entity: any = {
    address: {}
  };
  public plans;
  public id;
  public active;
  public payments;
  public onError = {
    cpf: false
  }
  public loading = false;
  public passwordConfirmation;
  @BlockUI() blockUI: NgBlockUI;

  constructor(
    public global: Global,
    public service: EvoService,
    public route: ActivatedRoute
  ) {
    this.getPlans();

    this.route.params.subscribe(params => {
      if (params.id) {
        this.id = params.id;
        this.get();
        this.getPaymentHistory();
      }
    })
  }

  ngOnInit(): void {

  }

  get() {
    this.service.entityName = 'clients';
    this.service.getResource(this.id).subscribe(response => {
      this.entity = response;
    })
  }

  getPlans() {
    this.service.entityName = 'plans';
    this.service.getResources().subscribe(response => {
      this.plans = response;
    })
  }

  getPaymentHistory() {
    this.service.entityName = `payments?user_id=${this.id}`;
    this.service.getResources().subscribe(response => {
      this.payments = response;
    })
  }

  onChangeCEP(cep) {
    this.blockUI.start('Buscando informações do endereço');
    this.global.useToken = false;
    this.service.getCep(cep).subscribe(
      (response: any) => {
        if (response) {
          this.entity.address.street = response.logradouro;
          this.entity.address.neighborhood = response.bairro;
        }
        this.global.useToken = true;
      },
      (error) => {
        this.blockUI.stop();
        this.global.iziToas.error({
          title: 'Ops',
          message: 'Ocorreu um problema ao buscar o cep informado.'
        });
        this.global.useToken = true;
      })
  }


  onChangeEmail(email) {
    if (!this.entity.id) {
      this.blockUI.start('Consultando informações...');
      this.onError.cpf = false;
      this.service.entityName = `validate-informations?field=email&value=${email}`;
      this.service.getResources().subscribe((response: any) => {
        this.blockUI.stop();
        if (response && response.result == true) {
          Swal.fire({
            title: 'Ops!',
            icon: 'warning',
            text: 'Já existe um usuário cadastrado com o E-mail informado, tente recuperar sua senha ou entre em contato com o suporte da Plataforma',
            confirmButtonText: 'Recuperar Senha',
          }).then(result => {
            if (!result.value) {
              //this.router.navigate(['/login'])
            }
          })
        }
      })
    }
  }

  onChangeWhatsapp(whatsapp) {
    if (whatsapp.length >= 11) {
      this.blockUI.start('Consultando informações...');
      this.onError.cpf = false;
      this.service.entityName = `validate-informations?field=phone&value=${whatsapp}`;
      this.service.getResources().subscribe((response: any) => {
        this.blockUI.stop();
        if (response && response.result == true) {
          Swal.fire({
            title: 'Ops!',
            icon: 'warning',
            text: 'Já existe um usuário cadastrado com o Telefone informado, tente recuperar sua senha ou entre em contato com o suporte da Plataforma',
            confirmButtonText: 'Recuperar Senha',
          }).then(result => {
            if (!result.value) {
              // this.router.navigate(['/login'])
            }
          })
        }
      })
    }
  }

  onChangeCPF(cpf) {
    if (cpf.length >= 11) {
      this.blockUI.start('Consultando informações...');
      if (this.global.cpfValidate(cpf)) {
        this.onError.cpf = false;
        this.service.entityName = `validate-informations?field=cpf&value=${cpf}`;
        this.service.getResources().subscribe((response: any) => {
          this.blockUI.stop();
          if (response && response.result == true) {
            Swal.fire({
              title: 'Ops!',
              icon: 'warning',
              text: 'Já existe um usuário cadastrado com o CPF informado, tente recuperar sua senha ou entre em contato com o suporte da Plataforma',
              confirmButtonText: 'Recuperar Senha',
            }).then(result => {
              if (!result.value) {
                //this.router.navigate(['/login'])
              }
            })
          }
        })
      }
      else {
        this.global.iziToas.error({
          title: 'O CPF informado é inválido'
        });
        this.onError.cpf = true;
        this.blockUI.stop();
      }
    }
  }

  save() {
    this.service.entityName = 'clients';
    this.entity.status = 1;
    this.service.createResource(this.entity).subscribe(
      response => {
        this.global.iziToas.show({
          title: 'Pronto',
          message: 'Cliente salvo com sucesso'
        })
      },
      error => {
        this.global.iziToas.error({
          title: 'Ops',
          message: 'Ocorreu um problema ao cadastrar este cliente'
        })
      })
  }

}
