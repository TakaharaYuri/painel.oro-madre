import { Component, OnInit } from '@angular/core';
import { Global } from 'src/app/@core/global';
import { EvoService } from 'src/app/@core/evo.service';
import { ActivatedRoute, Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {

  public key;
  public empresaRef;
  public data;

  constructor(
    public global: Global,
    public service: EvoService,
    public route: ActivatedRoute,
    public router: Router
  ) {
    this.route.queryParams.subscribe(param => {
      // if (param.key) {
      //   this.getData();
      // }
      // else {
      //   this.onValideError();
      // }
    });
  }

  ngOnInit() {
  }

  getData() {
    let data = {
      ref: this.empresaRef,
      key: this.key
    };
    this.service.entityName = `convite/validar`;
    this.service.createResource(data).subscribe(response => {
      this.data = response;
      localStorage.setItem('newUser', JSON.stringify(this.data));
      localStorage.setItem('step', '1');
    },
      error => {
        this.onValideError();
      });
  }

  onValideError() {
    Swal.fire({
      title: 'Ops!',
      icon: 'warning',
      text: 'O link do seu convite não é válido. Solicite ao seu gerente um novo convite.',
      showCancelButton: true,
      confirmButtonText: 'Solicitar novo Convite',
      cancelButtonText: 'Fechar',
    }).then(result => {
      if (!result.value) {
        this.router.navigate(['login'])
      }
    })
  }
}
