import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { EvoService } from 'src/app/@core/evo.service';
import { Global } from 'src/app/@core/global';
import { ModalCoinPairsComponent } from '../coin-pairs/modal-coin-pairs/modal-coin-pairs.component';
import { ModalGoalsDetailsComponent } from './modal-goals-details/modal-goals-details.component';

@Component({
  selector: 'app-analysis-details',
  templateUrl: './analysis-details.component.html',
  styleUrls: ['./analysis-details.component.css']
})
export class AnalysisDetailsComponent implements OnInit {

  public entity: any = {};
  public onLoadError = false;
  public leverage = [];
  public coinPairs;
  public selectedCoinPair;
  public now = Date.now();
  public users;
  public selectedUser;


  constructor(
    public service: EvoService,
    public global: Global,
    public route: ActivatedRoute,
    private modalService: NgbModal,
    public router: Router
  ) {

    this.getLeverage();
    this.getCoinPairs();
    this.getUsers();
    this.route.params.subscribe(params => {
      if (params.id) {
        this.entity.id = params.id;
      }
      else {
        this.entity = {
          analysis_permission: 'private',
          take_profit: [{ value: 0 }],
          market_type: 'avista',
          operation_price_type: 'sats'
        }
      }
    })
  }

  ngOnInit(): void {
    if (this.entity.id) {
      this.get(this.entity.id);
    }
  }

  get(id) {
    this.service.entityName = 'analysis-manager';
    this.service.getResource(this.entity.id).subscribe(response => {
      if (response) {
        this.entity = response;
      }
    })
  }
  save() {
    this.service.entityName = 'analysis-manager';
    if (!this.entity.id) {
      this.service.createResource(this.entity).subscribe((response: any) => {
        this.get(response.id);
        this.global.iziToas.success({ title: 'Análise salva com sucesso' });
        this.router.navigate(['/analysis-manager']);
      })
    }
    else {
      this.service.updateResource(this.entity).subscribe(response => {
        this.global.iziToas.success({ title: 'Análise atualizada com sucesso' });
      })
    }
    console.log(this.entity);
  }

  getCoinPairs() {
    this.service.entityName = 'coin-pairs';
    this.service.getResources().subscribe(response => {
      this.coinPairs = response;
    })
  }

  getLeverage() {
    const _array = new Array(125);
    for (let index = 0; index < _array.length; index++) {
      this.leverage.push(index + 1);
    }
  }

  addOption() {
    this.entity.take_profit.push({ value: 0 });
    console.log(this.entity.take_profit);
  }

  removeOption(index) {
    console.log(this.entity.take_profit, index);
    this.entity.take_profit.splice(index, 1);
  }

  onGoalChecked(index) {
    console.log('Goal ->', this.entity.take_profit[index]);
    if (this.entity.take_profit[index].goal == true) {
      this.entity.take_profit[index].goal_date = new Date();
    }
    else {
      delete (this.entity.take_profit[index].goal)
      delete (this.entity.take_profit[index].goal_date)
    }


    this.save();
  }

  onSelectCoinPairs(selected_id) {
    this.selectedCoinPair = this.coinPairs.find(item => item.id == selected_id);
  }

  addCoinPair(data = {}) {
    const modal = this.modalService.open(ModalCoinPairsComponent, {
      size: 'sm',
      keyboard: false,
      backdrop: 'static'
    });

    modal.componentInstance.entity = data;

    modal.result.then((result) => {
      if (result) {
        this.getCoinPairs();
        this.entity.coin_pairs_id = result.id;
      }
    });
  }

  addGoalInformation(item) {
    const modal = this.modalService.open(ModalGoalsDetailsComponent, {
      size: 'sm',
      keyboard: false,
      backdrop: 'static'
    });

    modal.componentInstance.entity = item;

    modal.result.then((result) => {
      if (result) {
        item = result;
        this.save();
      }
    });
  }

  setPermission(permission) {
    if (permission === 'public') {
      this.entity.analysis_permission = 'private';
    }
    else {
      this.entity.analysis_permission = 'public';
    }
  }

  getUsers() {
    this.service.entityName = 'users';
    this.service.getResources().subscribe(response => {
      this.users = response;
    })
  }

  onSelectUser(id) {
    this.selectedUser = this.users.find(item => item.id == id);
  }

  onChangeMarketType(marketType) {
    if (marketType == 'futuro') {
      this.entity.operation_price_type = 'dolar';
    }
    else {
      this.entity.operation_price_type = 'sats';
    }
  }

}
