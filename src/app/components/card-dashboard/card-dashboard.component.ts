import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-card-dashboard',
  templateUrl: './card-dashboard.component.html',
  styleUrls: ['./card-dashboard.component.css']
})
export class CardDashboardComponent implements OnInit {

  @Input() title = 'Título';
  @Input() value = 0;
  @Input() icon = 'fas fa-chart-bar';
  @Input() color = "bg-green";
  @Input() description = '';
  @Input() info;
  constructor() { }

  ngOnInit(): void {
  }

}
